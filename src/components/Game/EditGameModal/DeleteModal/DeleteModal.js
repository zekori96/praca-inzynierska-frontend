import React, { Component } from 'react';
import style from './DeleteModal.module.scss';
import Button from '../../../shared/Button/Button';
import { deleteGame } from '../../../../services/GamePageService/GamePageService';
import { withRouter } from 'react-router-dom';

export class DeleteModal extends Component {
  handleClick = () => {
    deleteGame(this.props.game.id).then(() => {
      this.props.history.push('/');
    });
  };

  render() {
    return (
      <div className={style.container}>
        <div className={style.content}>
          <img className={style.icon} src={this.props.text.icon} alt="warning" />
          <p className={style.description}>{this.props.text.warning}</p>
        </div>
        <Button onClick={this.handleClick} text={this.props.text.button.yes} />
        <Button onClick={this.props.onClose} text={this.props.text.button.no} />
      </div>
    );
  }
}

export default withRouter(DeleteModal);
