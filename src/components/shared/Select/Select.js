import React from 'react';
import style from './Select.module.scss';

export default props => {
  return (
    <div className={style.container}>
      <select onChange={props.changed} defaultValue={''}>
        <option value="" disabled>
          {props.placeholder}
        </option>
        {props.options.map((option, index) => (
          <option key={index} value={option.value}>
            {option.name}
          </option>
        ))}
      </select>
    </div>
  );
};
