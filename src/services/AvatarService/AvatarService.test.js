import React from 'react';
import { getAvatar } from './AvatarService';

describe('AvatarService', () => {
  describe('getAvatar', () => {
    it('should return avatar url', () => {
      const result = getAvatar('2');
      expect(result).toBe('https://avatars.dicebear.com/v2/identicon/:2.svg');
    });
  });
});
