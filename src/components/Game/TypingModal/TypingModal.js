import React, { Component } from 'react';
import style from './TypingModal.module.scss';
import Input from '../../shared/Input/Input';
import Button from '../../shared/Button/Button';
import { signIn, updateTypings } from '../../../services/GamePageService/GamePageService';
import BackButton from '../../shared/BackButton/BackButton';
import {
  checkIfNumberIsNotBlankAndNotNegative,
  checkIfNumberIsNotBlankAndPositive
} from '../../../services/ValidationService/ValidationService';
import { connect } from 'react-redux';

export class TypingModal extends Component {
  state = { matches: [] };

  componentDidMount() {
    this.setState({ game: this.props.game, matches: this.props.game.matchDtos });
  }

  updateTypings = () => {
    const typings = this.state.matches.map(match => ({
      id: match.id,
      score1: match.team1Score,
      score2: match.team2Score
    }));
    if (this.props.getUserType() === 'new') {
      const data = { score: this.state.points, typings: typings };
      return signIn(this.props.game.id, data).then(game => {
        this.props.refreshGameData(game);
        this.props.onClose();
      });
    } else {
      return updateTypings(this.props.game.id, { gameId: this.props.game.id, typings: typings })
        .then(game => this.props.refreshGameData(game))
        .then(this.props.onClose);
    }
  };

  handlePointsTyping = event => {
    let points = event.target.value;
    if (this.props.user.score < event.target.value) {
      points = this.props.user.score;
    }
    this.setState({ points: points });
  };

  handleChange = (event, identifier) => {
    const matches = this.state.game.matchDtos;
    const matchIndex = matches.findIndex(match => match.id === identifier.matchId);
    if (identifier.teamId === 1) {
      matches[matchIndex].team1Score = event.target.value;
    }
    if (identifier.teamId === 2) {
      matches[matchIndex].team2Score = event.target.value;
    }
    this.setState({ matches: matches });
  };

  checkIfDataIsCorrect() {
    const typings = this.state.matches.map(match => ({
      score1: match.team1Score,
      score2: match.team2Score
    }));
    let isDataCorrect = true;
    if (this.props.getUserType() === 'new') {
      isDataCorrect = isDataCorrect && checkIfNumberIsNotBlankAndPositive(this.state.points);
    }
    typings.forEach(match => {
      const { score1, score2 } = match;
      isDataCorrect =
        isDataCorrect && checkIfNumberIsNotBlankAndNotNegative(score1) && checkIfNumberIsNotBlankAndNotNegative(score2);
    });
    return isDataCorrect;
  }

  render() {
    return (
      <div className={style.container}>
        <BackButton className={style['back-button']} onClick={this.props.onClose} />
        <div className={style.content}>
          {this.props.getUserType() === 'new' ? (
            <div>
              <Input
                type="text"
                value={this.state.points}
                changed={this.handlePointsTyping}
                placeholder={this.props.text.joinPoints}
              />
            </div>
          ) : null}
          <div className={style.games}>
            {this.props.game.matchDtos.map((match, index) => (
              <div key={index}>
                <div>
                  <h2>{match.title}</h2>
                </div>
                <div>
                  <Input
                    type="text"
                    label={match.team1Name}
                    changed={event => this.handleChange(event, { matchId: match.id, teamId: 1 })}
                    value={match.team1Score || ''}
                  />
                  <Input
                    type="text"
                    label={match.team2Name}
                    changed={event => this.handleChange(event, { matchId: match.id, teamId: 2 })}
                    value={match.team2Score || ''}
                  />
                </div>
              </div>
            ))}
          </div>
        </div>
        <Button
          onClick={this.updateTypings}
          disabled={!this.checkIfDataIsCorrect()}
          text={this.props.text.button.save}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  const user = state.profile.user;

  return {
    user
  };
}

export default connect(mapStateToProps)(TypingModal);
