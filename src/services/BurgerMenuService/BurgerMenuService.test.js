import React from 'react';
import { getMenu } from './BurgerMenuService';

jest.mock('../contentfulClient', () => ({
  getContentfulEntries: jest.fn(() =>
    Promise.resolve({
      items: [
        {
          fields: {
            link: 'link1',
            name: 'name1',
            isForLoggedIn: true,
            priority: 1,
            icon: { fields: { file: { url: 'url1' } } },
            iconSelected: { fields: { file: { url: 'selectedUrl1' } } }
          }
        },
        {
          fields: {
            link: 'link2',
            name: 'name2',
            isForLoggedIn: true,
            priority: 2,
            icon: { fields: { file: { url: 'url2' } } },
            iconSelected: { fields: { file: { url: 'selectedUrl2' } } }
          }
        },
        {
          fields: {
            link: 'link3',
            name: 'name3',
            isForNotLoggedIn: true,
            priority: 3,
            icon: { fields: { file: { url: 'url3' } } },
            iconSelected: { fields: { file: { url: 'selectedUrl3' } } }
          }
        }
      ]
    })
  )
}));
describe('BurgerMenuService', () => {
  describe('getMenu', () => {
    it('should call getContentfulEntries and returned mapped version', async () => {
      const result = await getMenu(false);
      expect(result).toEqual([
        { icon: { notSelected: 'url3', selected: 'selectedUrl3' }, link: 'link3', name: 'name3' }
      ]);
    });
    it('should call getContentfulEntries and returned mapped version', async () => {
      const result = await getMenu(true);
      expect(result).toEqual([
        { icon: { notSelected: 'url1', selected: 'selectedUrl1' }, link: 'link1', name: 'name1' },
        { icon: { notSelected: 'url2', selected: 'selectedUrl2' }, link: 'link2', name: 'name2' }
      ]);
    });
  });
});
