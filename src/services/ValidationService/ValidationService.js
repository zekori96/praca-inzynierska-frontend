export function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

export function checkIfNumberIsNotBlankAndPositive(string) {
  return string != null && !isNaN(string) && string > 0;
}
export function checkIfNumberIsNotBlankAndNotNegative(string) {
  return string != null && !isNaN(string) && string >= 0;
}

export function checkIfStringIsNotBlank(string) {
  return string != null && string.trim().length > 0;
}

export function checkIfDateIsCorrect(date) {
  return date != null && date.isValid != null && date.isValid();
}
