import React from 'react';
import { Ranking } from './Ranking';
import { getRankingPage, getRankingRequest } from '../../services/RankingPageService/RankingPageService';

jest.mock('../../services/RankingPageService/RankingPageService', () => ({
  getRankingPage: jest.fn(() => Promise.resolve({ text: 'text' })),
  getRankingRequest: jest.fn(() => Promise.resolve([{ id: 'id' }]))
}));
describe('Ranking', () => {
  let ranking;
  const props = {};
  beforeEach(() => {
    ranking = new Ranking(props);
    ranking.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should call getRankingPage and getRankingResults then add result to state', async () => {
      await ranking.componentDidMount();
      expect(getRankingPage).toHaveBeenCalled();
      expect(getRankingRequest).toHaveBeenCalled();
      expect(ranking.setState).toHaveBeenCalledWith({ ranking: [{ id: 'id' }], text: { text: 'text' } });
    });
  });
  describe('handleRankingFilter', () => {
    it('should call getRankingResults with login and type if type is not null then add result to state', async () => {
      await ranking.handleRankingFilter('login', 1);
      expect(getRankingRequest).toHaveBeenCalledWith('login', 1);
      expect(ranking.setState).toHaveBeenCalledWith({
        ranking: [{ id: 'id' }],
        searchBox: 'login',
        selectedType: 1
      });
    });
    it('should call getRankingResults with login and selectedType if type is null then add result to state', async () => {
      ranking.state = { selectedType: 2 };
      await ranking.handleRankingFilter('login');
      expect(getRankingRequest).toHaveBeenCalledWith('login', 2);
      expect(ranking.setState).toHaveBeenCalledWith({
        ranking: [{ id: 'id' }],
        searchBox: 'login',
        selectedType: 2
      });
    });
  });
});
