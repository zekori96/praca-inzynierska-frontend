import React from 'react';
import style from './InfoTile.module.scss';

export default props => {
  return (
    <div className={style.content}>
      <span className={style['icon-border']}>
        <img className={style.icon} src={props.icon} alt={props.alt} />
      </span>
      <div className={style.info}>
        <h2 className={style.value}>{props.value}</h2>
        <p className={style.description}>{props.description}</p>
      </div>
    </div>
  );
};
