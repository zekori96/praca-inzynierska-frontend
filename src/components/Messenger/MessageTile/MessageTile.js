import React from 'react';
import style from './MessageTile.module.scss';
import moment from 'moment';

export default props => {
  const date = moment.unix(props.message.date);
  const content = props.message.invitation ? (
    <div className={style.invite} onClick={() => props.onInvitationClick(props.message.gameId)}>
      <img src={props.message.gameImageUrl} alt="invite" />
      <p className={style.title}>{props.message.gameTitle}</p>
      <p>{props.message.gameDescription}</p>
    </div>
  ) : (
    <p>{props.message.text}</p>
  );
  return (
    <div className={[style.message, props.message.received ? style.received : style.sent].join(' ')}>
      <div className={style.content}>{content}</div>
      <div>{date.isSame(moment(), 'd') ? date.format('HH:mm') : date.format('DD.MM.YYYY HH:mm')}</div>
    </div>
  );
};
