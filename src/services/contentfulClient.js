import * as contentful from 'contentful';

const contentfulClient = contentful.createClient({
  space: '142cra9r5o2b',
  accessToken: '13396e6328e36bd1a6270b4f8380a22e50aa85955fe705ccadf44ef33faf863d'
});

export const getContentfulEntry = param => {
  return contentfulClient.getEntry(param);
};

export function getAdditionalTexts() {
  return getContentfulEntry('4VkdB7ZmQUAWCke8KgquAA').then(({ fields }) => fields);
}

export const getContentfulEntries = params => {
  return contentfulClient.getEntries({ ...params, include: 2 });
};
