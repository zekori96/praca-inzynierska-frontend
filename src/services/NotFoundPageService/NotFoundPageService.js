import { getContentfulEntry } from '../contentfulClient';

export function getNotFoundPage() {
  return getContentfulEntry('7nHo5cFUSAmKcYQQsq80aI').then(({ fields }) => {
    return {
      icon: fields.warningIcon.fields.file.url,
      text: fields.warningText,
      button: fields.goToMainPageButton
    };
  });
}
