import { getContentfulEntries } from '../contentfulClient';

export function getMenu(isLoggedIn) {
  return getContentfulEntries({ content_type: 'menuItem' }).then(({ items }) =>
    items
      .filter(item => (isLoggedIn ? !!item.fields.isForLoggedIn : !!item.fields.isForNotLoggedIn))
      .sort((a, b) => (a.fields.priority > b.fields.priority ? 1 : b.fields.priority > a.fields.priority ? -1 : 0))
      .map(item => ({
        icon: {
          notSelected: item.fields.icon.fields.file.url,
          selected: item.fields.iconSelected.fields.file.url
        },
        link: item.fields.link,
        name: item.fields.name
      }))
  );
}
