import React from 'react';
import { Register } from './Register';
import { getRegisterPage, sendRegisterRequest } from '../../services/RegisterPageService/RegisterPageService';
import { checkIfStringIsNotBlank, validateEmail } from '../../services/ValidationService/ValidationService';

jest.mock('../../services/RegisterPageService/RegisterPageService', () => ({
  getRegisterPage: jest.fn(() => Promise.resolve({ text: 'text' })),
  sendRegisterRequest: jest.fn(() => Promise.resolve({ id: 'id' }))
}));
jest.mock('../../services/ValidationService/ValidationService', () => ({
  checkIfStringIsNotBlank: jest.fn(() => true),
  validateEmail: jest.fn(() => true)
}));
describe('Register', () => {
  let register;
  const props = { logIn: jest.fn(), history: { push: jest.fn() } };
  beforeEach(() => {
    register = new Register(props);
    register.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should call getRegisterPage then add result to state', async () => {
      await register.componentDidMount();
      expect(getRegisterPage).toHaveBeenCalled();
      expect(register.setState).toHaveBeenCalledWith({ text: 'text' });
    });
  });

  describe('inputChangedHandler', () => {
    it('should add event value to state with identifier as key', async () => {
      await register.inputChangedHandler({ target: { value: 'value' } }, 'identifier');
      expect(register.setState).toHaveBeenCalledWith({
        registerForm: {
          email: '',
          identifier: 'value',
          login: '',
          password: '',
          repeatPassword: ''
        }
      });
    });
  });

  describe('registerHandler', () => {
    it('should prevent default, clear error message, then call sendRegisterRequest if form is correct', async () => {
      const event = { target: { value: 'value' }, preventDefault: jest.fn() };
      register.clearErrorMessage = jest.fn();
      register.checkIfFormIsCorrect = jest.fn(() => ({ isCorrect: true, type: [] }));
      register.state = { registerForm: { login: 'login', password: 'password', email: 'email' } };
      await register.registerHandler(event);
      expect(event.preventDefault).toHaveBeenCalled();
      expect(register.clearErrorMessage).toHaveBeenCalled();
      expect(register.checkIfFormIsCorrect).toHaveBeenCalled();
      expect(sendRegisterRequest).toHaveBeenCalledWith('login', 'password', 'email');
      expect(props.logIn).toHaveBeenCalledWith({ id: 'id' });
      expect(props.history.push).toHaveBeenCalledWith('/');
    });

    it('should call showServerError if sendRegisterRequest fail', async () => {
      const event = { target: { value: 'value' }, preventDefault: jest.fn() };
      register.clearErrorMessage = jest.fn();
      register.checkIfFormIsCorrect = jest.fn(() => ({ isCorrect: true, type: [] }));
      register.showServerError = jest.fn();
      jest.spyOn(global.console, 'error').mockImplementation(() => {});
      sendRegisterRequest.mockReturnValue(Promise.reject());
      register.state = { registerForm: { login: 'login', password: 'password', email: 'email' } };
      await register.registerHandler(event);
      expect(event.preventDefault).toHaveBeenCalled();
      expect(register.clearErrorMessage).toHaveBeenCalled();
      expect(register.checkIfFormIsCorrect).toHaveBeenCalled();
      expect(sendRegisterRequest).toHaveBeenCalledWith('login', 'password', 'email');
      expect(register.showServerError).toHaveBeenCalled();
    });

    it('should call showPasswordLengthError if form is not correct and checkIfFormIsCorrect returned type 1', async () => {
      const event = { target: { value: 'value' }, preventDefault: jest.fn() };
      register.clearErrorMessage = jest.fn();
      register.checkIfFormIsCorrect = jest.fn(() => ({ isCorrect: false, type: [1] }));
      register.showPasswordLengthError = jest.fn();
      register.form = { reset: jest.fn() };
      await register.registerHandler(event);
      expect(event.preventDefault).toHaveBeenCalled();
      expect(register.clearErrorMessage).toHaveBeenCalled();
      expect(register.checkIfFormIsCorrect).toHaveBeenCalled();
      expect(register.showPasswordLengthError).toHaveBeenCalled();
      expect(register.form.reset).toHaveBeenCalled();
    });

    it('should call showPasswordsNotEqualError if form is not correct and checkIfFormIsCorrect returned type 2', async () => {
      const event = { target: { value: 'value' }, preventDefault: jest.fn() };
      register.clearErrorMessage = jest.fn();
      register.checkIfFormIsCorrect = jest.fn(() => ({ isCorrect: false, type: [2] }));
      register.showPasswordsNotEqualError = jest.fn();
      register.form = { reset: jest.fn() };
      await register.registerHandler(event);
      expect(event.preventDefault).toHaveBeenCalled();
      expect(register.clearErrorMessage).toHaveBeenCalled();
      expect(register.checkIfFormIsCorrect).toHaveBeenCalled();
      expect(register.showPasswordsNotEqualError).toHaveBeenCalled();
      expect(register.form.reset).toHaveBeenCalled();
    });

    it('should call showWrongEmailError if form is not correct and checkIfFormIsCorrect returned type 3', async () => {
      const event = { target: { value: 'value' }, preventDefault: jest.fn() };
      register.clearErrorMessage = jest.fn();
      register.checkIfFormIsCorrect = jest.fn(() => ({ isCorrect: false, type: [3] }));
      register.showWrongEmailError = jest.fn();
      register.form = { reset: jest.fn() };
      await register.registerHandler(event);
      expect(event.preventDefault).toHaveBeenCalled();
      expect(register.clearErrorMessage).toHaveBeenCalled();
      expect(register.checkIfFormIsCorrect).toHaveBeenCalled();
      expect(register.showWrongEmailError).toHaveBeenCalled();
      expect(register.form.reset).toHaveBeenCalled();
    });
  });

  describe('checkIfFormIsCorrect', () => {
    it('should return true if all data are correct', () => {
      register.state = {
        registerForm: {
          login: 'login',
          password: 'password',
          email: 'email@email.com',
          repeatPassword: 'password'
        }
      };
      const result = register.checkIfFormIsCorrect();
      expect(result).toEqual({ isCorrect: true, type: [] });
    });
    it('should return false with code 4 if checkIfStringIsNotBlank return false for login', () => {
      checkIfStringIsNotBlank.mockImplementation(string => string !== '');
      register.state = {
        registerForm: { login: '', password: 'password', email: 'email@email.com', repeatPassword: 'password' }
      };
      const result = register.checkIfFormIsCorrect();
      expect(result).toEqual({ isCorrect: false, type: [4] });
    });
    it('should return false with code 4 if checkIfStringIsNotBlank return false for password', () => {
      checkIfStringIsNotBlank.mockImplementation(string => string !== '');
      register.state = {
        registerForm: { login: 'login', password: '', email: 'email@email.com', repeatPassword: '' }
      };
      const result = register.checkIfFormIsCorrect();
      expect(result).toEqual({ isCorrect: false, type: [4, 1] });
    });
    it('should return false with code 4 if checkIfStringIsNotBlank return false for email', () => {
      checkIfStringIsNotBlank.mockImplementation(string => string !== '');
      register.state = {
        registerForm: { login: 'login', password: 'password', email: '', repeatPassword: 'password' }
      };
      const result = register.checkIfFormIsCorrect();
      expect(result).toEqual({ isCorrect: false, type: [4] });
    });
    it('should return false with code 4 if checkIfStringIsNotBlank return false for repeatPassword', () => {
      checkIfStringIsNotBlank.mockImplementation(string => string !== '');
      register.state = {
        registerForm: { login: 'login', password: '', email: 'email@email.com', repeatPassword: '' }
      };
      const result = register.checkIfFormIsCorrect();
      expect(result).toEqual({ isCorrect: false, type: [4, 1] });
    });
    it('should return false with code 1 if password is shorter than 8 characters', () => {
      register.state = {
        registerForm: { login: 'login', password: 'pass', email: 'email@email.com', repeatPassword: 'pass' }
      };
      const result = register.checkIfFormIsCorrect();
      expect(result).toEqual({ isCorrect: false, type: [1] });
    });
    it('should return false with code 2 if password is not equal to repeatPassword', () => {
      register.state = {
        registerForm: { login: 'login', password: 'password', email: 'email@email.com', repeatPassword: 'pass' }
      };
      const result = register.checkIfFormIsCorrect();
      expect(result).toEqual({ isCorrect: false, type: [2] });
    });
    it('should return false with code 3 if email is not valid', () => {
      validateEmail.mockImplementation(string => string !== 'email');
      register.state = {
        registerForm: { login: 'login', password: 'password', email: 'email', repeatPassword: 'password' }
      };
      const result = register.checkIfFormIsCorrect();
      expect(result).toEqual({ isCorrect: false, type: [3] });
    });
  });

  describe('clearErrorMessage', () => {
    it('should clear errorMessage in state', async () => {
      register.state = { errorMessage: [{ error: 'error' }] };
      await register.clearErrorMessage();
      expect(register.setState).toHaveBeenCalledWith({ errorMessage: [] });
    });
  });

  describe('showServerError', () => {
    it('should add server error to state', async () => {
      register.state = { errorMessage: [] };
      await register.showServerError();
      expect(register.setState).toHaveBeenCalledWith({ errorMessage: [<p />] });
    });
  });

  describe('showPasswordLengthError', () => {
    it('should add password length error to state', async () => {
      register.state = { errorMessage: [] };
      await register.showPasswordLengthError();
      expect(register.setState).toHaveBeenCalledWith({ errorMessage: [<p />] });
    });
  });

  describe('showPasswordsNotEqualError', () => {
    it('should add password not equal error to state', async () => {
      register.state = { errorMessage: [] };
      await register.showPasswordsNotEqualError();
      expect(register.setState).toHaveBeenCalledWith({ errorMessage: [<p />] });
    });
  });

  describe('showWrongEmailError', () => {
    it('should add wrong email error to state', async () => {
      register.state = { errorMessage: [] };
      await register.showWrongEmailError();
      expect(register.setState).toHaveBeenCalledWith({ errorMessage: [<p />] });
    });
  });
});
