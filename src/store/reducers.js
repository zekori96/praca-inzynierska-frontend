import { combineReducers } from 'redux';
import { profile } from './profile/profile.reducer';

export default combineReducers({
  profile
});
