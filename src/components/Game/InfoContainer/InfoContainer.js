import React from 'react';
import style from '../../../containers/Game/Game.module.scss';
import moment from 'moment';

export default props => {
  return (
    <div className={style['info-container']}>
      <div className={style.info}>
        <div>
          <p className={style.header}>{props.text.endDate}</p>
          <p className={style.value}>{moment.unix(props.game.endDate).format('DD.MM.YYYY HH:mm')}</p>
        </div>
        <div>
          <p className={style.header}>{props.text.created}</p>
          <p className={style.value}>{moment.unix(props.game.createdDate).format('DD.MM.YYYY HH:mm')}</p>
        </div>
        <div>
          <p className={style.header}>{props.text.creator}</p>
          <p className={style.value}>{props.game.creatorLogin}</p>
        </div>
        <div>
          <p className={style.header}>{props.text.users}</p>
          <p className={style.value}>{props.game.users}</p>
        </div>
        <div>
          <p className={style.header}>{props.text.points}</p>
          <p className={style.value}>{props.game.points}</p>
        </div>
      </div>
    </div>
  );
};
