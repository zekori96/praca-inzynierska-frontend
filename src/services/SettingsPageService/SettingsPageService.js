import { getContentfulEntry } from '../contentfulClient';
import { instance } from '../AxiosService/AxiosService';

export function getSettingsPage() {
  return getContentfulEntry('IE0UTVHOSas4qG2KOeKWG').then(({ fields }) => {
    return {
      settings: {
        text: {
          email: { icon: fields.emailIcon.fields.file.url, text: fields.emailInformationText },
          login: { icon: fields.loginIcon.fields.file.url, text: fields.loginInformationText },
          score: { icon: fields.scoreIcon.fields.file.url, text: fields.scoreInformationText }
        },
        button: { edit: fields.editButtonText, delete: fields.deleteButtonText }
      },
      edit: {
        text: {
          newPassword: fields.editNewPasswordText,
          oldPassword: fields.editOldPasswordText,
          info: fields.editInfoText
        },
        button: fields.editConfirmButtonText,
        error: {
          passwordLengthErrorMessage: fields.passwordLengthErrorMessage,
          emailErrorMessage: fields.emailErrorMessage,
          serverErrorMessage: fields.serverErrorMessage
        }
      },
      delete: {
        icon: fields.deleteIcon.fields.file.url,
        text: fields.deleteInformationText,
        button: { yes: fields.deleteYesButton, no: fields.deleteNoButton }
      }
    };
  });
}

export function sendEditedProfile(editedData) {
  return instance.post('/user', { ...editedData }).then(response => response.data);
}

export function deleteProfile() {
  return instance.delete('/user').then(response => response.data);
}
