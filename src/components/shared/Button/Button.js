import React from 'react';
import style from './Button.module.scss';

export default props => {
  return (
    <button
      className={style.button}
      onClick={props.onClick || null}
      type={props.type || null}
      disabled={props.disabled || false}
    >
      {props.text}
    </button>
  );
};
