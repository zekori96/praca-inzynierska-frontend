# ekaBet frontend

## Scope of software

Front-end part of the application that gives ability to make and participate in sports betting games.

## Goals

TBD.

## Non-goals

1. This is not a bookmaker application.

## Environments

Application is automatically deployed via Codeship (https://app.codeship.com/sportsBroker) on:

1.  Dev server (TBA) after every push to master branch
2.  Stage server (TBA) after every push to stage branch
3.  Prod server (TBA) after every push to release branch

## Technical information

### Sub-systems

1. Docker
1. React

### Prerequisites

1. NodeJS (Latest)
1. Docker (Latest)
1. Docker Compose (Latest)

### Supported systems

1. Windows 10
1. Linux
1. MacOS

### How to build the project

In main directory run:

- `npm install` to download dependencies
- `npm run build` to build the project

### How to run using Docker

1. In the main project's directory run `docker-compose up`

### How to run the application locally (for developers)

In main directory run `npm start`.

### How to test project

#### Unit tests

In the main project's directory run `npm test`.

#### Integration tests

For integration testing you need to install cypress globally and run a mock version of the app:

- `npm i -g cypress@3.1.0`
- `npm run startMock`

In the main project's directory run:

- `npm run integration` for headless integration tests
- `npm run integration-headed` to see integration tests in the browser

Note that to run integration tests you need start application first.
