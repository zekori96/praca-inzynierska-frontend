import React from 'react';
import style from './BackButton.module.scss';
import Button from '../Button/Button';

export default props => {
  return (
    <div className={[style['back-button-container'], props.className].join(' ')}>
      <Button onClick={props.onClick} text={'<'} />
    </div>
  );
};
