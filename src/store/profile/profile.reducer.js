import { LOGIN, LOGOUT } from '../profile/profile.actions';

const defaultState = { user: undefined };

export const profile = (state = defaultState, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        user: action.payload
      };
    case LOGOUT:
      return defaultState;
    default:
      return state;
  }
};
