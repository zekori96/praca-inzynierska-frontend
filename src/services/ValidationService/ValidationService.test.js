import { checkIfDateIsCorrect, checkIfNumberIsNotBlankAndPositive, checkIfStringIsNotBlank } from './ValidationService';
import * as moment from 'moment';

describe('ValidationService', () => {
  describe('checkIfNumberIsNotBlankAndPositive', () => {
    it('should return true if string is not blank', async () => {
      expect(checkIfNumberIsNotBlankAndPositive('1')).toBe(true);
      expect(checkIfNumberIsNotBlankAndPositive(' 1 ')).toBe(true);
      expect(checkIfNumberIsNotBlankAndPositive('            1')).toBe(true);
      expect(checkIfNumberIsNotBlankAndPositive('1           ')).toBe(true);
    });
    it('should return false if string is empty', async () => {
      expect(checkIfNumberIsNotBlankAndPositive('')).toBe(false);
      expect(checkIfNumberIsNotBlankAndPositive('       ')).toBe(false);
      expect(checkIfNumberIsNotBlankAndPositive(null)).toBe(false);
      expect(checkIfNumberIsNotBlankAndPositive(undefined)).toBe(false);
    });
  });

  describe('checkIfStringIsNotBlank', () => {
    it('should return true if string is not blank', async () => {
      expect(checkIfStringIsNotBlank('string')).toBe(true);
      expect(checkIfStringIsNotBlank(' string ')).toBe(true);
      expect(checkIfStringIsNotBlank('            string')).toBe(true);
      expect(checkIfStringIsNotBlank('string           ')).toBe(true);
    });
    it('should return false if string is empty', async () => {
      expect(checkIfStringIsNotBlank('')).toBe(false);
      expect(checkIfStringIsNotBlank('       ')).toBe(false);
      expect(checkIfStringIsNotBlank(null)).toBe(false);
      expect(checkIfStringIsNotBlank(undefined)).toBe(false);
    });
  });

  describe('checkIfDateIsCorrect', () => {
    it('should return true if date is correct', async () => {
      expect(checkIfDateIsCorrect(moment.unix('1547936058'))).toBe(true);
    });
    it('should return false if date is not correct', async () => {
      expect(checkIfDateIsCorrect('')).toBe(false);
      expect(checkIfDateIsCorrect('safa')).toBe(false);
    });
  });
});
