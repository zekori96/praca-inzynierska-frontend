import React from 'react';
import { Game } from './Game';
import { getGame, getGamePage, signOut } from '../../services/GamePageService/GamePageService';

jest.mock('../../services/GamePageService/GamePageService', () => ({
  getGamePage: jest.fn(() => Promise.resolve({ text: 'text' })),
  getGame: jest.fn(() => Promise.resolve({ game: 'game' })),
  signOut: jest.fn(() => Promise.resolve({ game: 'game' }))
}));
describe('Game', () => {
  let game;
  const props = {
    history: { push: jest.fn() },
    location: { pathname: 'xxx/yyy/zzz/id' }
  };
  beforeEach(() => {
    game = new Game(props);
    game.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should add gameId, text and game data to state', async () => {
      game.refreshGameData = jest.fn();
      await game.componentDidMount();
      expect(getGamePage).toHaveBeenCalled();
      expect(getGame).toHaveBeenCalled();
      expect(game.setState).toHaveBeenCalledWith({ gameId: 'id', text: { text: 'text' } });
      expect(game.refreshGameData).toHaveBeenCalledWith({ game: 'game' });
    });
    it('should return to home page if getGame returns error', async () => {
      game.refreshGameData = jest.fn();
      getGame.mockReturnValue(Promise.reject());
      await game.componentDidMount();
      expect(getGamePage).toHaveBeenCalled();
      expect(getGame).toHaveBeenCalled();
      expect(game.setState).toHaveBeenCalledWith({ gameId: 'id', text: { text: 'text' } });
      expect(props.history.push).toHaveBeenCalledWith('/');
    });
  });

  describe('componentDidUpdate', () => {
    //TODO: Create tests when functionality is done.
  });

  describe('refreshGameData', () => {
    it('should save game to state', async () => {
      await game.refreshGameData({ matchDtos: [{ typed: true }, { typed: false }] });
      expect(game.setState).toHaveBeenCalledWith({
        game: { matchDtos: [{ typed: true }, { typed: false }] },
        typedGames: [{ typed: true }]
      });
    });
  });
  describe('closeModal', () => {
    it('should set modal state as close ', async () => {
      await game.closeModal();
      expect(game.setState).toHaveBeenCalledWith({ modal: { isOpen: false, type: '' } });
    });
  });

  describe('handleLeaveButtonClick', () => {
    it('should call signOut with gameId then call refreshGameData ', async () => {
      game.refreshGameData = jest.fn();
      game.state = { gameId: 'id' };
      await game.handleLeaveButtonClick();
      expect(signOut).toHaveBeenCalledWith('id');
      expect(game.refreshGameData).toHaveBeenCalledWith({ game: 'game' });
    });
  });

  describe('handleTypeButtonClick', () => {
    it('should set modal state as open and type as new ', async () => {
      await game.handleTypeButtonClick();
      expect(game.setState).toHaveBeenCalledWith({ modal: { type: 'new', isOpen: true } });
    });
  });

  describe('handleInviteButtonClick', () => {
    it('should redirect to conversation url with gameId', async () => {
      game.state = { gameId: 'id' };
      await game.handleInviteButtonClick();
      expect(props.history.push).toHaveBeenCalledWith('/conversation/new?gameId=id');
    });
  });

  describe('handleEditButtonClick', () => {
    it('should set modal state as open and type as edit ', async () => {
      await game.handleEditButtonClick();
      expect(game.setState).toHaveBeenCalledWith({ modal: { type: 'edit', isOpen: true } });
    });
  });

  describe('handleEditModalClick', () => {
    it('should set modal state as open and type as event-modal ', async () => {
      await game.handleEditModalClick('event');
      expect(game.setState).toHaveBeenCalledWith({ modal: { type: 'event-modal', isOpen: true } });
    });
  });
});
