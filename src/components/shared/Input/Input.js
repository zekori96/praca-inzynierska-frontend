import React from 'react';
import style from './Input.module.scss';

export default props => {
  const label = props.label ? props.label : '';
  const placeholder = props.placeholder ? props.placeholder : '';
  return (
    <div className={[style.container, props.className].join(' ')}>
      {props.label ? <label>{label}</label> : null}
      <input
        type={props.type}
        onChange={props.changed}
        value={props.value}
        placeholder={placeholder}
        onKeyPress={props.onKeyPress}
      />
    </div>
  );
};
