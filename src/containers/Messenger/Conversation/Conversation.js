import React, { Component } from 'react';
import style from './Conversation.module.scss';
import Input from '../../../components/shared/Input/Input';
import Avatar from '../../../components/shared/Avatar/Avatar';
import BackButton from '../../../components/shared/BackButton/BackButton';
import {
  getConversationPage,
  getMessages,
  sendNewMessage
} from '../../../services/ConversationService/ConversationService';
import MessageTile from '../../../components/Messenger/MessageTile/MessageTile';
import { withRouter } from 'react-router-dom';
import Button from '../../../components/shared/Button/Button';
import { connect } from 'react-redux';

export class Conversation extends Component {
  newMessage = React.createRef();
  state = {
    user: {
      id: '',
      login: ''
    },
    messages: [],
    placeholder: '',
    newMessage: '',
    conversationId: ''
  };

  componentDidMount() {
    const id = this.props.location.pathname.substr(this.props.location.pathname.lastIndexOf('/') + 1);
    getConversationPage().then(state => this.setState({ ...this.state, conversationId: id, ...state }));
    this.updateMessages(id);
  }

  updateMessages = id => {
    getMessages(id).then(conversation => {
      this.setState({
        user: { id: conversation.userId, login: conversation.login },
        messages: conversation.messages
      });
      this.newMessage.current.scrollIntoView({ behavior: 'smooth' });
    });
  };

  handleNewMessage = event => {
    this.setState({ newMessage: event.target.value });
  };

  handleKeyPress = event => {
    if (event.key === 'Enter') {
      sendNewMessage(this.state.conversationId, this.state.newMessage).then(() => {
        this.setState({ newMessage: '' });
        this.updateMessages(this.state.conversationId);
      });
    }
  };
  handleRefresh = () => {
    this.updateMessages(this.state.conversationId);
  };

  render() {
    return (
      <div className={style.container}>
        <div className={style['user-panel']}>
          <BackButton className={style['back-button']} onClick={this.props.history.goBack} />
          <Avatar className={style.avatar} id={this.state.user.id} />
          <p className={style.login}>{this.state.user.login}</p>
        </div>
        {this.state.messages.map((message, index) => (
          <MessageTile key={index} message={message} onInvitationClick={id => this.props.history.push(`/game/${id}`)} />
        ))}
        <div className={style['new-message']} ref={this.newMessage}>
          <div className={style['new-message-field']}>
            <Input
              className={style.input}
              placeholder={this.state.placeholder}
              value={this.state.newMessage}
              changed={this.handleNewMessage}
              onKeyPress={this.handleKeyPress}
            />
            <img src={this.state.send} alt="send message" onClick={() => this.handleKeyPress({ key: 'Enter' })} />
          </div>
          <Button onClick={this.handleRefresh} text={this.state.refresh} />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const user = state.profile.user;
  return {
    user
  };
}

export default withRouter(connect(mapStateToProps)(Conversation));
