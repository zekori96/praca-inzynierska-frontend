import React, { Component } from 'react';
import style from './NewGame.module.scss';
import { createGame, getNewGamePage } from '../../services/NewGamePageService/NewGamePageService';
import Input from '../../components/shared/Input/Input';
import Select from '../../components/shared/Select/Select';
import PlayoffCreator from '../../components/NewGame/PlayoffCreator/PlayoffCreator';
import Button from '../../components/shared/Button/Button';
import DateTimeSelector from '../../components/shared/DateTimeSelector/DateTimeSelector';
import * as moment from 'moment';
import {
  checkIfDateIsCorrect,
  checkIfNumberIsNotBlankAndPositive,
  checkIfStringIsNotBlank
} from '../../services/ValidationService/ValidationService';

export class NewGame extends Component {
  state = {
    text: { button: {}, team: {}, tooMuchMatchesErrorMessage: '', serverErrorMessage: '' },
    playoffCreators: [],
    errorMessage: [],
    gameTypes: [
      { name: 'piłka nożna', value: 'football' },
      { name: 'koszykówka', value: 'basketball' },
      { name: 'tenis', value: 'tennis' },
      { name: 'golf', value: 'golf' },
      { name: 'inny', value: 'other' }
    ],
    inputs: { numberOfPlayoffs: 0, matches: [], endOfGameDate: null }
  };

  componentDidMount() {
    getNewGamePage().then(state => this.setState({ text: state }));
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.inputs.numberOfPlayoffs !== this.state.inputs.numberOfPlayoffs) {
      this.getPlayoffCreators();
    }
  }

  getPlayoffCreators() {
    const numberOfCreatorsToCreate = this.state.inputs.numberOfPlayoffs - this.state.playoffCreators.length;
    if (numberOfCreatorsToCreate > 0) {
      this.addSetNumberOfPlayoffCreators(numberOfCreatorsToCreate);
    } else if (numberOfCreatorsToCreate < 0) {
      this.deleteSetNumberOfPlayoffCreators(Math.abs(numberOfCreatorsToCreate));
    }
  }

  addSetNumberOfPlayoffCreators(number) {
    const playoffCreators = this.state.playoffCreators;
    for (let i = 0; i < number; i++) {
      playoffCreators.push(
        <PlayoffCreator
          key={this.state.playoffCreators.length}
          index={this.state.playoffCreators.length}
          text={this.state.text}
          onClick={this.handleInput}
          endGameDate={this.state.inputs.endOfGameDate}
        />
      );
    }
    this.setState({ playoffCreators: playoffCreators });
  }

  deleteSetNumberOfPlayoffCreators(number) {
    const playoffCreators = this.state.playoffCreators;
    const matches = this.state.inputs.matches;
    for (let i = 0; i < number; i++) {
      playoffCreators.pop();
      matches.pop();
    }
    this.setState({ playoffCreators: playoffCreators, inputs: { ...this.state.inputs, matches: matches } });
  }

  handleInput = (value, identifier, matchId) => {
    let save;
    if (identifier.startsWith('match-')) {
      const newMatches = this.state.inputs.matches;
      newMatches.splice(matchId, 1, { ...newMatches[matchId], [identifier.slice(6, identifier.length)]: value });
      save = { ...this.state.inputs, matches: newMatches };
    } else {
      save = { ...this.state.inputs, [identifier]: value };
    }
    this.setState({ inputs: save });
  };

  handleNumberOfMatchesChange = event => {
    if (!checkIfNumberIsNotBlankAndPositive(event.target.value)) {
      return;
    }
    if (event.target.value <= 10) {
      this.setState({
        inputs: { ...this.state.inputs, numberOfPlayoffs: event.target.value },
        errorMessage: [...this.state.errorMessage.filter(err => err.key !== 'tooMuchMatchesErrorMessage')]
      });
    } else {
      if (this.state.errorMessage.find(err => err.key === 'tooMuchMatchesErrorMessage') === undefined) {
        this.appendTooMuchMatchesErrorMessage();
      }
      this.setState({
        inputs: { ...this.state.inputs, numberOfPlayoffs: 10 }
      });
    }
  };

  appendTooMuchMatchesErrorMessage() {
    return this.setState({
      errorMessage: [
        ...this.state.errorMessage,
        {
          key: 'tooMuchMatchesErrorMessage',
          value: <p key={this.state.errorMessage.length}>{this.state.text.tooMuchMatchesErrorMessage}</p>
        }
      ]
    });
  }

  handleCreateButton = () => {
    const data = {
      title: this.state.inputs.title,
      description: this.state.inputs.description,
      type: this.state.inputs.gameType,
      endDate: this.state.inputs.endOfGameDate,
      matches: this.state.inputs.matches
    };

    return createGame(data)
      .then(gameId => this.props.history.push(`/game/${gameId}`))
      .catch(error => {
        console.error(error);
        this.setState({
          errorMessage: [
            ...this.state.errorMessage,
            { key: 'serverErrorMessage', value: <p>{this.state.text.serverErrorMessage}</p> }
          ]
        });
      });
  };

  canCreateGame() {
    const { title, description, gameType, endOfGameDate, matches, numberOfPlayoffs } = this.state.inputs;
    const isTitleCorrect = checkIfStringIsNotBlank(title);
    const isDescriptionCorrect = checkIfStringIsNotBlank(description);
    const isGameTypeCorrect = checkIfStringIsNotBlank(gameType);
    const isEndOfGameDateCorrect = checkIfDateIsCorrect(endOfGameDate);
    const isNumberOfPlayoffsCorrect = numberOfPlayoffs > 0;
    const areMatchesCorrect = this.checkIfMatchesAreCorrect(matches);
    return (
      isTitleCorrect &&
      isDescriptionCorrect &&
      isGameTypeCorrect &&
      isEndOfGameDateCorrect &&
      isNumberOfPlayoffsCorrect &&
      areMatchesCorrect
    );
  }

  checkIfMatchesAreCorrect(matches) {
    let areMatchesDataCorrect = matches.length > 0;
    matches.forEach(match => {
      const { date, team1Name, team2Name, title } = match;
      areMatchesDataCorrect =
        areMatchesDataCorrect &&
        checkIfDateIsCorrect(date) &&
        checkIfStringIsNotBlank(team1Name) &&
        checkIfStringIsNotBlank(team2Name) &&
        checkIfStringIsNotBlank(title);
    });
    return areMatchesDataCorrect;
  }

  render() {
    return (
      <div className={style.container}>
        {this.state.errorMessage ? (
          <div className={style.errorMessage}>{this.state.errorMessage.map(err => err.value)}</div>
        ) : null}
        <div className={style.input}>
          <Input placeholder={this.state.text.title} changed={event => this.handleInput(event.target.value, 'title')} />
        </div>
        <div className={style.input}>
          <Input
            placeholder={this.state.text.description}
            changed={event => this.handleInput(event.target.value, 'description')}
          />
        </div>
        <div className={[style.input]}>
          <Select
            options={this.state.gameTypes}
            placeholder={this.state.text.gameType}
            changed={event => this.handleInput(event.target.value, 'gameType')}
          />
        </div>
        <div className={style.input}>
          <DateTimeSelector
            onChange={this.handleInput}
            date={this.state.text.endOfGameDate}
            identifier={'endOfGameDate'}
            enableAfter={moment()}
          />
        </div>
        <div className={style.input}>
          <Input placeholder={this.state.text.numberOfPlayoffs} changed={this.handleNumberOfMatchesChange} />
        </div>
        <div className={style.playoff}>{this.state.playoffCreators}</div>
        <div className={style.button}>
          <Button
            onClick={this.handleCreateButton}
            disabled={!this.canCreateGame()}
            text={this.state.text.button.create}
          />
        </div>
      </div>
    );
  }
}
