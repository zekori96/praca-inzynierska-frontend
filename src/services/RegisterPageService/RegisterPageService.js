import { getContentfulEntry } from '../contentfulClient';
import { instance } from '../AxiosService/AxiosService';
import { sendLogInRequest } from '../LogInPageService/LogInPageService';

export function getRegisterPage() {
  return getContentfulEntry('1rKtzse0DmOMOAMQMUUga0').then(({ fields }) => ({
    header: fields.header,
    description: fields.description,
    serverErrorMessage: fields.serverErrorMessage,
    passwordLengthErrorMessage: fields.passwordLengthErrorMessage,
    passwordsNotEqualErrorMessage: fields.passwordsNotEqualErrorMessage,
    wrongEmailErrorMessage: fields.wrongEmailErrorMessage,
    text: {
      input: {
        login: fields.loginText,
        password: fields.passwordText,
        repeatPassword: fields.repeatPasswordText,
        email: fields.emailText
      },
      button: { register: fields.registerButtonText, logIn: fields.logInButtonText }
    }
  }));
}

export function sendRegisterRequest(login, password, email) {
  return instance
    .post('/register', {
      login: login,
      password: password,
      email: email
    })
    .then(() => sendLogInRequest(login, password));
}
