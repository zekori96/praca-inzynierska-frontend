import React from 'react';
import { ScrollToTop } from './ScrollToTop';
import { isUserLoggedIn } from '../../services/LogInPageService/LogInPageService';
import { instance } from '../../services/AxiosService/AxiosService';

jest.mock('../../services/LogInPageService/LogInPageService', () => ({
  isUserLoggedIn: jest.fn(() => true)
}));
jest.mock('../../services/AxiosService/AxiosService', () => ({
  instance: {
    get: jest.fn(() => Promise.resolve({ data: 'data' }))
  }
}));
describe('ScrollToTop', () => {
  let scrollToTop;
  const props = { logIn: jest.fn(), logOut: jest.fn(), history: { push: jest.fn() }, location: 'location' };
  beforeEach(() => {
    scrollToTop = new ScrollToTop(props);
    scrollToTop.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should get user from backend then save it in redux if user is logged in', async () => {
      await scrollToTop.componentDidMount();
      expect(instance.get).toHaveBeenCalledWith('/user');
      expect(props.logIn).toHaveBeenCalledWith('data');
    });
    it('should logOut, remove authorization key from localStorage then redirect to home page ', async () => {
      isUserLoggedIn.mockReturnValue(false);
      await scrollToTop.componentDidMount();
      expect(props.logOut).toHaveBeenCalled();
      expect(localStorage.removeItem).toHaveBeenCalledWith('auth');
      expect(props.history.push).toHaveBeenCalledWith('/');
    });
  });
  // describe('componentDidUpdate', () => {
  //   afterEach(() => {
  //     jest.clearAllMocks();
  //   });
  //   it('should scroll to top if location is not equal to prev location', async () => {
  //     await scrollToTop.componentDidUpdate({ location: 'prevLocation' });
  //     expect(window.scrollTo).toHaveBeenCalledWith(0, 0);
  //   });
  //   it('should not scroll to top if location is equal to prev location', async () => {
  //     await scrollToTop.componentDidUpdate({ location: 'location' });
  //     expect(window.scrollTo).not.toHaveBeenCalled();
  //   });
  // });
});
