import React, { Component } from 'react';
import style from './Edit.module.scss';
import Input from '../../shared/Input/Input';
import Button from '../../shared/Button/Button';
import BackButton from '../../shared/BackButton/BackButton';
import { checkIfStringIsNotBlank, validateEmail } from '../../../services/ValidationService/ValidationService';

export default class Edit extends Component {
  state = {
    email: '',
    password: '',
    oldPassword: '',
    errorMessage: []
  };

  componentDidMount() {
    this.setState({
      email: this.props.email,
      password: this.props.text.newPassword,
      oldPassword: this.props.text.oldPassword
    });
  }

  onEmailChange = event => {
    this.setState({ email: event.target.value });
  };

  onPasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  onOldPasswordChange = event => {
    this.setState({ oldPassword: event.target.value });
  };

  checkIfSaveDataAreCorrect() {
    const { email, password, oldPassword } = this.state;
    let isCorrect = true;
    const types = [];
    if (
      !(checkIfStringIsNotBlank(email) && checkIfStringIsNotBlank(password) && checkIfStringIsNotBlank(oldPassword))
    ) {
      isCorrect = false;
      types.push(3);
    }
    if (password.length < 8) {
      isCorrect = false;
      types.push(1);
    }
    if (!validateEmail(email)) {
      isCorrect = false;
      types.push(2);
    }
    return { isCorrect: isCorrect, type: types };
  }

  appendPasswordLengthError() {
    this.setState({
      errorMessage: [...this.state.errorMessage, <p>{this.props.text.passwordLengthErrorMessage}</p>]
    });
  }

  appendEmailError() {
    this.setState({
      errorMessage: [...this.state.errorMessage, <p>{this.props.text.emailErrorMessage}</p>]
    });
  }

  appendServerError() {
    this.setState({
      errorMessage: [...this.state.errorMessage, <p>{this.props.text.serverErrorMessage}</p>]
    });
  }

  handleSave = () => {
    this.setState({ errorMessage: [] });
    const isCorrect = this.checkIfSaveDataAreCorrect();
    if (isCorrect.isCorrect) {
      this.props.updateProfile(this.state.email, this.state.password, this.state.oldPassword).catch(error => {
        this.appendServerError();
        console.error(error);
      });
    } else {
      if (isCorrect.type.includes(1)) {
        this.appendPasswordLengthError();
      }
      if (isCorrect.type.includes(2)) {
        this.appendEmailError();
      }
    }
  };

  render() {
    return (
      <div className={style.edit}>
        <BackButton className={style['back-button']} onClick={this.props.close} />
        <div className={style.content}>
          {this.state.errorMessage ? <div className={style.errorMessage}>{this.state.errorMessage}</div> : null}
          <div className={style.inputs}>
            <Input type="email" changed={this.onEmailChange} value={this.state.email} />
            <Input type="password" changed={this.onPasswordChange} placeholder={this.state.password} />
            <Input type="password" changed={this.onOldPasswordChange} placeholder={this.state.oldPassword} />
            <p className={style.description}>{this.props.text.description}</p>
          </div>
        </div>
        <Button onClick={this.handleSave} text={this.props.text.buttonAccept} />
      </div>
    );
  }
}
