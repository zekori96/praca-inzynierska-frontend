import React from 'react';
import style from './WarningModal.model.scss';
import Button from '../Button/Button';
//TODO: Check if we need this.
export default props => {
  return (
    <div className={style.delete}>
      <div className={style.content}>
        <img className={style.icon} src={props.warning} alt="warning" />
        <p className={style.description}>{props.details}</p>
      </div>
      <Button onClick={props.delete} text={props.buttonYes} />
      <Button onClick={props.close} text={props.buttonNo} />
    </div>
  );
};
