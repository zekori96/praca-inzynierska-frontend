import React, { Component } from 'react';
import style from './EndModal.module.scss';
import Input from '../../../shared/Input/Input';
import Button from '../../../shared/Button/Button';
import { endGame } from '../../../../services/GamePageService/GamePageService';
import BackButton from '../../../shared/BackButton/BackButton';
import { checkIfNumberIsNotBlankAndNotNegative } from '../../../../services/ValidationService/ValidationService';

export default class EndModal extends Component {
  state = {};
  handleSave = () => {
    const data = Object.keys(this.state).map(key => ({ id: key, ...this.state[key] }));
    endGame(this.props.game.id, data).then(game => {
      this.props.refreshGameData(game);
      this.props.onClose();
    });
  };

  handleChange = (event, matchId, identifier) => {
    this.setState({ [matchId]: { ...this.state[matchId], [identifier]: event } });
  };

  checkIfDataIsCorrect() {
    const data = Object.keys(this.state).map(key => ({ id: key, ...this.state[key] }));
    let isDataCorrect = Object.keys(this.state).length === this.props.game.matchDtos.length;
    data.forEach(match => {
      const { team1, team2 } = match;
      isDataCorrect =
        isDataCorrect && checkIfNumberIsNotBlankAndNotNegative(team1) && checkIfNumberIsNotBlankAndNotNegative(team2);
    });
    return isDataCorrect;
  }

  render() {
    console.log(this.props.game);
    return (
      <div className={style.container}>
        <BackButton className={style['back-button']} onClick={this.props.onClose} />
        {this.props.game.matchDtos.map((match, index) => (
          <div key={index}>
            <h2>{match.title}</h2>
            <div>
              <Input
                label={match.team1Name}
                type="text"
                changed={event => this.handleChange(event.target.value, match.id, 'team1')}
                placeholder={this.props.text.team1Points}
              />
            </div>
            <div>
              <Input
                label={match.team2Name}
                type="text"
                changed={event => this.handleChange(event.target.value, match.id, 'team2')}
                placeholder={this.props.text.team2Points}
              />
            </div>
          </div>
        ))}
        <div className={style.content}>
          <img className={style.icon} src={this.props.text.icon} alt="warning" />
          <p className={style.description}>{this.props.text.warning}</p>
        </div>
        <Button onClick={this.handleSave} disabled={!this.checkIfDataIsCorrect()} text={this.props.text.button.yes} />
        <Button onClick={this.props.onClose} text={this.props.text.button.no} />
      </div>
    );
  }
}
