import React from 'react';
import { NewMessage } from './NewMessage';
import {
  getConversation,
  getNewMessagePage,
  getUsers,
  sendInvite
} from '../../../services/NewMessageService/NewMessageService';

jest.mock('../../../services/NewMessageService/NewMessageService', () => ({
  getNewMessagePage: jest.fn(() => Promise.resolve({ text: 'text' })),
  getUsers: jest.fn(() => Promise.resolve([{ id: 'id' }])),
  sendInvite: jest.fn(() => Promise.resolve('conversationId')),
  getConversation: jest.fn(() => Promise.resolve('conversationId'))
}));
jest.mock('qs', () => ({
  parse: jest.fn(() => ({ gameId: 'id' }))
}));
describe('NewMessage', () => {
  let newMessage;
  const props = {
    history: { push: jest.fn() },
    location: { pathname: 'xxx/yyy/zzz?gameId=id' }
  };
  beforeEach(() => {
    newMessage = new NewMessage(props);
    newMessage.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should call getNewMessagePage and getUsers then add result to state with gameId', async () => {
      newMessage.updateMessages = jest.fn();
      await newMessage.componentDidMount();
      expect(getNewMessagePage).toHaveBeenCalled();
      expect(getUsers).toHaveBeenCalled();
      expect(newMessage.setState).toHaveBeenCalledWith({
        gameId: 'id',
        search: '',
        text: 'text',
        users: [{ id: 'id' }]
      });
    });
  });

  describe('handleUserClick', () => {
    it('should call sendInvite if gameId exists then redirect to conversation with conversationId', async () => {
      newMessage.state = { gameId: 'id' };
      await newMessage.handleUserClick('userId');
      expect(sendInvite).toHaveBeenCalledWith('userId', 'id');
      expect(props.history.push).toHaveBeenCalledWith('/conversation/conversationId');
    });
    it("should call getConversation if gameId doesn't exists then redirect to conversation with conversationId", async () => {
      newMessage.state = {};
      await newMessage.handleUserClick('userId');
      expect(getConversation).toHaveBeenCalledWith('userId');
      expect(props.history.push).toHaveBeenCalledWith('/conversation/conversationId');
    });
  });

  describe('handleSearch', () => {
    it('should call getUsers with event value then add users to state', async () => {
      await newMessage.handleSearch({ target: { value: 'value' } });
      expect(getUsers).toHaveBeenCalledWith('value');
      expect(newMessage.setState).toHaveBeenCalledWith({ users: [{ id: 'id' }] });
    });
  });
});
