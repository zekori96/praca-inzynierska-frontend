import React from 'react';
import { Conversation } from './Conversation';
import {
  getConversationPage,
  getMessages,
  sendNewMessage
} from '../../../services/ConversationService/ConversationService';

jest.mock('../../../services/ConversationService/ConversationService', () => ({
  getConversationPage: jest.fn(() => Promise.resolve({ text: 'text' })),
  getMessages: jest.fn(() => Promise.resolve({ userId: 'id', login: 'login', messages: [{ message: 'message' }] })),
  sendNewMessage: jest.fn(() => Promise.resolve())
}));
describe('Conversation', () => {
  let conversation;
  const props = {
    isLoggedIn: true,
    logIn: jest.fn(),
    history: { push: jest.fn() },
    location: { pathname: 'xxx/yyy/zzz/id' }
  };
  beforeEach(() => {
    conversation = new Conversation(props);
    conversation.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should call getConversationPage then add result to state with conversationId', async () => {
      conversation.updateMessages = jest.fn();
      await conversation.componentDidMount();
      expect(getConversationPage).toHaveBeenCalled();
      expect(conversation.setState).toHaveBeenCalledWith({
        conversationId: 'id',
        messages: [],
        newMessage: '',
        placeholder: '',
        text: 'text',
        user: { id: '', login: '' }
      });
    });
  });

  describe('updateMessages', () => {
    it('should call getMessages then add result to state and scroll to last message', async () => {
      conversation.newMessage = { current: { scrollIntoView: jest.fn() } };
      await conversation.updateMessages('id');
      expect(getMessages).toHaveBeenCalledWith('id');
      expect(conversation.setState).toHaveBeenCalledWith({
        user: { id: 'id', login: 'login' },
        messages: [{ message: 'message' }]
      });
      expect(conversation.newMessage.current.scrollIntoView).toHaveBeenCalledWith({ behavior: 'smooth' });
    });
  });

  describe('handleNewMessage', () => {
    it('should add event value to state as new message', async () => {
      await conversation.handleNewMessage({ target: { value: 'value' } });
      expect(conversation.setState).toHaveBeenCalledWith({ newMessage: 'value' });
    });
  });

  describe('handleKeyPress', () => {
    it('should call sendNewMessage then set newMessage as empty and update messages if event key is Enter', async () => {
      conversation.state = { conversationId: 'id', newMessage: 'newMessage' };
      conversation.updateMessages = jest.fn();
      await conversation.handleKeyPress({ key: 'Enter' });
      expect(sendNewMessage).toHaveBeenCalledWith('id', 'newMessage');
      expect(conversation.setState).toHaveBeenCalledWith({ newMessage: '' });
      expect(conversation.updateMessages).toHaveBeenCalledWith('id');
    });
  });

  describe('handleRefresh', () => {
    it('should call updateMessages with conversationId', async () => {
      conversation.state = { conversationId: 'id' };
      conversation.updateMessages = jest.fn();
      await conversation.handleRefresh();
      expect(conversation.updateMessages).toHaveBeenCalledWith('id');
    });
  });
});
