const webpack = require('./config-webpack.default');

const escapeRegExp = s => s.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');

module.exports = config => {
  const newConfig = config;
  const aliasMapper = Object.keys(webpack.resolve.alias).reduce(
    (previous, current) =>
      Object.assign(previous, {
        [escapeRegExp(current)]: webpack.resolve.alias[current]
      }),
    {
      '^@(.*)$': '<rootDir>/src/$1'
    }
  );

  delete aliasMapper.app;

  newConfig.moduleNameMapper = Object.assign(config.moduleNameMapper, aliasMapper);

  return newConfig;
};
