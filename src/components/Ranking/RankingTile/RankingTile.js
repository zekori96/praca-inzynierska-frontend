import React from 'react';
import style from './RankingTile.module.scss';
import { getAvatar } from '../../../services/AvatarService/AvatarService';
import { toMomentFormat } from '../../../services/MomentService/MomentService';

export default props => {
  return (
    <div className={style.user}>
      <img src={getAvatar(props.user.id)} alt="user avatar" />
      <div className={style.info}>
        <h2>{props.user.login}</h2>
        <div className={style.wrapper}>
          <p>{`${props.text.lastLoggedIn} `}</p>
          <p>{`${toMomentFormat(props.user.lastLoggedIn)}`}</p>
        </div>
        <p>{`${props.text.score} ${props.user.score}pkt`}</p>
      </div>
    </div>
  );
};
