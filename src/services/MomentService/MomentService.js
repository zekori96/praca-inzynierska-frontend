import * as moment from 'moment';

export function toMomentFormat(time) {
  const date = moment.unix(time);
  if (date.isSame(new Date(), 'day')) {
    return date.format('HH:mm');
  } else if (date.isSame(new Date(), 'year')) {
    return date.format('HH:mm DD.MM');
  } else {
    return date.format('HH:mm DD.MM.YYYY');
  }
}
