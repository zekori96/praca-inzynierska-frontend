import { getContentfulEntry } from '../contentfulClient';
import { instance } from '../AxiosService/AxiosService';

export function getMessengerPage() {
  return getContentfulEntry('zf6vZ9mIY8OKUG0wwquge').then(({ fields }) => {
    return {
      newMessage: fields.newMessageButtonText,
      search: fields.searchText,
      invitation: fields.invitationText
    };
  });
}

export function getConversations(searchValue) {
  return instance.get('/conversation', { params: { login: searchValue } }).then(response => response.data);
}
