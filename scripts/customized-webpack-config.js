const rewire = require('rewire');
const proxyquire = require('proxyquire');
const packageName = 'react-scripts';

function loadCustomizer(module) {
  try {
    return require(module);
  } catch (e) {
    if (e.code !== 'MODULE_NOT_FOUND') {
      throw e;
    }
  }
  return config => config;
}

function rewireModule(modulePath, customizer) {
  const defaults = rewire(modulePath);
  const config = defaults.__get__('config');
  customizer(config);
}

switch (process.argv[2]) {
  case 'start':
    rewireModule(`${packageName}/scripts/start.js`, loadCustomizer('../config/config-webpack.development'));
    break;
  case 'build':
    rewireModule(`${packageName}/scripts/build.js`, loadCustomizer('../config/config-webpack.production'));
    break;
  case 'test':
    const customizer = loadCustomizer('../config/config-webpack.testing');
    proxyquire('react-scripts/scripts/test.js', {
      './utils/createJestConfig': (...args) => {
        var createJestConfig = require(`${packageName}/scripts/utils/createJestConfig`);
        return customizer(createJestConfig(...args));
      }
    });
    break;
  default:
    console.log('Only supports "start", "build", and "test" options.');
    process.exit(-1);
}
