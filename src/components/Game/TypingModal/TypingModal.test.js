import React from 'react';
import TypingModal from './TypingModal';
import { signIn, updateTypings } from '../../../services/GamePageService/GamePageService';

jest.mock('../../../services/GamePageService/GamePageService', () => ({
  signIn: jest.fn(() => Promise.resolve({ game: 'game' })),
  updateTypings: jest.fn(() => Promise.resolve({ game: 'game' }))
}));
describe('TypingModal', () => {
  let typingModal;
  const props = {
    game: { id: 'id', matchDtos: [{ matchId: 'id' }] },
    refreshGameData: jest.fn(),
    onClose: jest.fn(),
    getUserType: jest.fn(() => 'new')
  };
  beforeEach(() => {
    typingModal = new TypingModal(props);
    typingModal.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should add game and matchDtos from props to state', async () => {
      await typingModal.componentDidMount();
      expect(typingModal.setState).toHaveBeenCalledWith({
        game: { id: 'id', matchDtos: [{ matchId: 'id' }] },
        matches: [{ matchId: 'id' }]
      });
    });
  });

  describe('updateTypings', () => {
    it('should call signIn if user is new then refresh game and close modal', async () => {
      typingModal.state = { points: 12, matches: [{ id: 'id', team1Score: 2, team2Score: 1 }] };
      typingModal.props.getUserType = jest.fn(() => 'new');
      await typingModal.updateTypings();
      expect(signIn).toHaveBeenCalledWith('id', { score: 12, typings: [{ id: 'id', score1: 2, score2: 1 }] });
      expect(props.refreshGameData).toHaveBeenCalledWith({ game: 'game' });
      expect(props.onClose).toHaveBeenCalled();
    });
    it('should call updateTypings if user is not new then refresh game and close modal', async () => {
      typingModal.state = { matches: [{ id: 'id', team1Score: 2, team2Score: 1 }] };
      typingModal.props.getUserType = jest.fn(() => 'owner');
      await typingModal.updateTypings();
      expect(updateTypings).toHaveBeenCalledWith('id', { gameId: 'id', typings: [{ id: 'id', score1: 2, score2: 1 }] });
      expect(props.refreshGameData).toHaveBeenCalledWith({ game: 'game' });
      expect(props.onClose).toHaveBeenCalled();
    });
  });

  describe('handlePointsTyping', () => {
    it('should add points to state', async () => {
      await typingModal.handlePointsTyping({ target: { value: 1 } });
      expect(typingModal.setState).toHaveBeenCalledWith({ points: 1 });
    });
  });
  describe('handleChange', () => {
    it('should update matches', async () => {
      typingModal.state = { game: { matchDtos: [{ id: 'id', team1Score: 1, team2Score: 2 }] } };
      await typingModal.handleChange({ target: { value: '2' } }, { matchId: 'id', teamId: 1 });
      expect(typingModal.setState).toHaveBeenCalledWith({ matches: [{ id: 'id', team1Score: '2', team2Score: 2 }] });
    });
  });

  describe('checkIfDataIsCorrect', () => {
    it('should return true if all data is correct', () => {
      typingModal.state = { points: 1, matches: [{ id: 'id', team1Score: 1, team2Score: 2 }] };
      typingModal.checkIfNumberIsNotBlankAndPositive = jest.fn(() => true);
      const result = typingModal.checkIfDataIsCorrect();
      expect(result).toBe(true);
    });
    it('should return false if points are not correct', () => {
      typingModal.state = { points: '', matches: [{ id: 'id', team1Score: 1, team2Score: 2 }] };
      typingModal.props.getUserType = jest.fn(() => 'new');
      typingModal.checkIfNumberIsNotBlankAndPositive = jest.fn(string => string !== '');
      const result = typingModal.checkIfDataIsCorrect();
      expect(result).toBe(false);
    });
    it('should return false if team1Score is not correct', () => {
      typingModal.state = { points: 1, matches: [{ id: 'id', team1Score: '', team2Score: 2 }] };
      typingModal.checkIfNumberIsNotBlankAndPositive = jest.fn(string => string !== '');
      const result = typingModal.checkIfDataIsCorrect();
      expect(result).toBe(false);
    });
    it('should return false if team2Name is not correct', () => {
      typingModal.state = { points: 1, matches: [{ id: 'id', team1Score: 1, team2Score: '' }] };
      typingModal.checkIfNumberIsNotBlankAndPositive = jest.fn(string => string !== '');
      const result = typingModal.checkIfDataIsCorrect();
      expect(result).toBe(false);
    });
  });
});
