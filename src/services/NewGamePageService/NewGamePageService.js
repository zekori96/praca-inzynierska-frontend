import { getContentfulEntry } from '../contentfulClient';
import { instance } from '../AxiosService/AxiosService';

export function getNewGamePage() {
  return getContentfulEntry('jOuYeW5RC0sKeaG0KiMmM').then(({ fields }) => ({
    button: {
      create: fields.createButtonText
    },
    title: fields.title,
    description: fields.description,
    endOfGameDate: fields.endOfGameDate,
    gameType: fields.gameType,
    numberOfPlayoffs: fields.numberOfPlayoffs,
    reminderDate: fields.reminderDate,
    playoff: fields.playoff,
    playoffDate: fields.playoffDate,
    team: {
      team1: fields.team1,
      team2: fields.team2
    },
    tooMuchMatchesErrorMessage: fields.tooMuchMatchesErrorMessage,
    serverErrorMessage: fields.serverErrorMessage
  }));
}

export function createGame(data) {
  return instance.post(`/game`, data).then(response => {
    return response.data;
  });
}
