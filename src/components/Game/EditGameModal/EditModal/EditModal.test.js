import React from 'react';
import EditModal from './EditModal';
import { updateGameInfo } from '../../../../services/GamePageService/GamePageService';

jest.mock('../../../../services/GamePageService/GamePageService', () => ({
  updateGameInfo: jest.fn(() => Promise.resolve({ game: 'game' }))
}));
describe('EditModal', () => {
  let editModal;
  const props = {
    game: {
      id: 'id',
      title: 'title',
      description: 'description'
    },
    refreshGameData: jest.fn(),
    onClose: jest.fn()
  };
  beforeEach(() => {
    editModal = new EditModal(props);
    editModal.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should add title and description from props to state', async () => {
      await editModal.componentDidMount();
      expect(editModal.setState).toHaveBeenCalledWith({ title: 'title', description: 'description' });
    });
  });

  describe('handleChange', () => {
    it('should add event to state with identifier as key', async () => {
      await editModal.handleChange('event', 'identifier');
      expect(editModal.setState).toHaveBeenCalledWith({ identifier: 'event' });
    });
  });
  describe('handleSave', () => {
    it('should call updateGameInfo with game id and updated title and description then call refreshGameData and close modal', async () => {
      editModal.state = { title: 'title', description: 'description' };
      await editModal.handleSave();
      expect(updateGameInfo).toHaveBeenCalledWith('id', { title: 'title', description: 'description' });
      expect(props.refreshGameData).toHaveBeenCalledWith({ game: 'game' });
      expect(props.onClose).toHaveBeenCalled();
    });
  });
  describe('checkIfDataIsCorrect', () => {
    it('should return true if checkIfStringIsNotBlank returns true for title and description', () => {
      editModal.state = { title: 'title', description: 'description' };
      editModal.checkIfStringIsNotBlank = jest.fn(() => true);
      const result = editModal.checkIfDataIsCorrect();
      expect(result).toBe(true);
    });
    it('should return false if checkIfStringIsNotBlank returns false for title', () => {
      editModal.state = { title: '', description: 'description' };
      editModal.checkIfStringIsNotBlank = jest.fn(string => string !== '');
      const result = editModal.checkIfDataIsCorrect();
      expect(result).toBe(false);
    });
    it('should return false if checkIfStringIsNotBlank returns false for description', () => {
      editModal.state = { title: 'title', description: '' };
      editModal.checkIfStringIsNotBlank = jest.fn(string => string !== '');
      const result = editModal.checkIfDataIsCorrect();
      expect(result).toBe(false);
    });
    it('should return false if checkIfStringIsNotBlank returns false for title and description', () => {
      editModal.state = { title: '', description: '' };
      editModal.checkIfStringIsNotBlank = jest.fn(string => string !== '');
      const result = editModal.checkIfDataIsCorrect();
      expect(result).toBe(false);
    });
  });
});
