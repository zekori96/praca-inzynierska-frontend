import React from 'react';
import style from './UserTile.module.scss';
import { getAvatar } from '../../../../services/AvatarService/AvatarService';

export default props => {
  return (
    <div className={style.content} onClick={() => props.onClick(props.userId)}>
      <img className={style.avatar} src={getAvatar(props.userId)} alt="avatar" />
      <div className={style['info-container']}>
        <h2 className={style.login}>{props.login}</h2>
      </div>
    </div>
  );
};
