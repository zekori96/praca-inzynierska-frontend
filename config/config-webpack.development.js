const webpackConfig = require('./config-webpack.default');

module.exports = config => {
  const { resolve } = config;
  resolve.alias = Object.assign(config.resolve.alias, webpackConfig.resolve.alias);
  const eslintLoader = config.module.rules[0];
  eslintLoader.use[0].options.emitWarning = true;
  eslintLoader.use[0].options.useEslintrc = true;
  const loaderList = config.module.rules[1].oneOf;
  webpackConfig.rules[0].oneOf.forEach(customRule => {
    const ruleIdx = loaderList.findIndex(rule => rule.test === customRule.test);
    loaderList.splice(ruleIdx || loaderList.length - 1, 0, customRule);
  });
};
