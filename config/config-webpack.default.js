const path = require('path');

module.exports = {
  resolve: {
    alias: {
      '@': path.resolve(__dirname, '../src')
    }
  },
  rules: [
    {
      oneOf: [
        {
          test: /^(?=.*\.scss$)(?:(?!\.module\.).)+$/,
          use: [
            {
              loader: 'style-loader'
            },
            {
              loader: 'css-loader'
            },
            {
              loader: 'sass-loader',
              options: {
                includePaths: [path.join(__dirname, '..', 'node_modules')]
              }
            }
          ]
        },
        {
          test: /\.module.scss$/,
          use: [
            {
              loader: 'style-loader'
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
                modules: true,
                localIdentName: '[local]___[hash:base64:5]'
              }
            },
            {
              loader: 'sass-loader',
              options: {
                includePaths: [path.join(__dirname, '..', 'node_modules'), path.join(__dirname, '..', 'src')]
              }
            }
          ]
        }
      ]
    }
  ]
};
