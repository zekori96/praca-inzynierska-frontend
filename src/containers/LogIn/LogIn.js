import React, { Component } from 'react';
import { getLogInPage, sendLogInRequest } from '../../services/LogInPageService/LogInPageService';
import style from './LogIn.module.scss';
import Input from '../../components/shared/Input/Input';
import { connect } from 'react-redux';
import { logIn } from '../../store/profile/profile.actions';
import { withRouter } from 'react-router-dom';
import Button from '../../components/shared/Button/Button';
import { checkIfStringIsNotBlank } from '../../services/ValidationService/ValidationService';

export class LogIn extends Component {
  state = {
    header: '',
    description: '',
    text: {
      button: { logIn: '', register: '' },
      input: { login: '', password: '' }
    },
    logInForm: {
      login: '',
      password: ''
    }
  };
  form;

  componentDidMount() {
    getLogInPage().then(state => this.setState(state));
  }

  inputChangedHandler = (event, inputIdentifier) => {
    this.setState({
      logInForm: {
        ...this.state.logInForm,
        [inputIdentifier]: event.target.value
      }
    });
  };
  logInHandler = event => {
    event.preventDefault();
    if (this.checkIfFormIsCorrect()) {
      return sendLogInRequest(this.state.logInForm.login, this.state.logInForm.password)
        .then(profile => {
          this.props.logIn(profile);
          this.props.history.push('/');
        })
        .catch(error => {
          this.showWrongDataErrorMessage();
          console.error(error);
        });
    } else {
      this.showWrongDataErrorMessage();
    }
  };

  checkIfFormIsCorrect() {
    return (
      checkIfStringIsNotBlank(this.state.logInForm.login) && checkIfStringIsNotBlank(this.state.logInForm.password)
    );
  }

  showWrongDataErrorMessage() {
    this.setState({ errorMessage: this.state.wrongDataErrorMessage });
    this.form.reset();
  }

  render() {
    return (
      <div className={style.container}>
        <h1>{this.state.header}</h1>
        <p>{this.state.description}</p>
        {this.state.errorMessage ? <p className={style.errorMessage}>{this.state.errorMessage}</p> : null}
        <form onSubmit={this.logInHandler} ref={el => (this.form = el)}>
          <Input
            type="text"
            label={this.state.text.input.login}
            changed={event => this.inputChangedHandler(event, 'login')}
          />
          <Input
            type="password"
            label={this.state.text.input.password}
            changed={event => this.inputChangedHandler(event, 'password')}
          />
          <Button type="submit" text={this.state.text.button.logIn} />
        </form>
        <Button onClick={() => this.props.history.push('/register')} text={this.state.text.button.register} />
      </div>
    );
  }
}

export default withRouter(
  connect(
    null,
    { logIn }
  )(LogIn)
);
