import { getContentfulEntry } from '../contentfulClient';
import { instance } from '../AxiosService/AxiosService';
import * as JWT from 'jwt-decode';
import * as moment from 'moment';

export function getLogInPage() {
  return getContentfulEntry('2mp0J8RDmMIUYo6eCwYkKK').then(({ fields }) => ({
    header: fields.header,
    description: fields.description,
    wrongDataErrorMessage: fields.wrongDataErrorMessage,
    text: {
      input: { login: fields.loginText, password: fields.passwordText },
      button: { logIn: fields.acceptButtonText, register: fields.registerButtonText }
    }
  }));
}

export const sendLogInRequest = (login, password) => {
  return instance
    .post('/login', { login: login, password: password })
    .then(response => {
      localStorage.setItem('auth', response.headers.authorization);
    })
    .then(() => instance.get('/user').then(response => response.data));
};
export const isUserLoggedIn = () => {
  return localStorage.getItem('auth') != null && moment.unix(JWT(localStorage.getItem('auth')).exp).isAfter(moment());
};
