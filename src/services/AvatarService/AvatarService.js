export function getAvatar(id) {
  return `https://avatars.dicebear.com/v2/identicon/:${id}.svg`;
}
