import { getContentfulEntry } from '../contentfulClient';
import { instance } from '../AxiosService/AxiosService';

export function getRankingPage() {
  return getContentfulEntry('giwK4QdEWsq8KUKWik0C4').then(({ fields }) => ({
    search: {
      icon: fields.searchIcon.fields.file.url,
      text: fields.searchText,
      button: {
        today: fields.searchButtonToday,
        thisWeek: fields.searchButtonThisWeek,
        thisMonth: fields.searchButtonThisMonth,
        thisYear: fields.searchButtonThisYear,
        tenBest: fields.searchButtonTenBest,
        all: fields.searchButtonAll
      }
    },
    user: {
      lastLoggedIn: fields.userTextLastLoggedIn,
      score: fields.userTextScore
    }
  }));
}

export function getRankingRequest(login = '', typeId = 5) {
  return instance.get('/ranking', { params: { login: login, typeId: typeId } }).then(response => {
    return response.data.data;
  });
}
