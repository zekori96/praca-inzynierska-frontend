import React, { Component } from 'react';
import { Bar } from 'react-chartjs-2';

export class Stats extends Component {
  componentDidMount() {
    this.getDeltas();
  }

  getDeltas() {
    const realScoreDelta = Number.parseInt(this.props.stats.realTeam1Score - this.props.stats.realTeam2Score, 10);
    const scores = [realScoreDelta];
    scores.push(...Object.keys(this.props.stats.scoreDelta).map(number => Number.parseInt(number, 10)));
    scores.sort((a, b) => a - b);
    return scores;
  }

  getPopulations() {
    return this.getDeltas().map(key => this.props.stats.scoreDelta[key]);
  }

  getOfficialScoreValues() {
    return this.getDeltas().map(
      score =>
        Number.parseInt(score, 10) ===
        Number.parseInt(this.props.stats.realTeam1Score - this.props.stats.realTeam2Score, 10)
          ? 1
          : 0
    );
  }

  data = {
    labels: this.getDeltas(),
    datasets: [
      {
        label: 'Delta typowań',
        backgroundColor: '#004182',
        borderColor: '#002040',
        borderWidth: 1,
        hoverBackgroundColor: '#002040',
        hoverBorderColor: '#002040',
        data: this.getPopulations()
      },
      {
        label: 'Oficjalna delta',
        backgroundColor: '#02fdff',
        borderColor: '#02bfc1',
        borderWidth: 1,
        hoverBackgroundColor: '#02bfc1',
        hoverBorderColor: '#02bfc1',
        data: this.getOfficialScoreValues('realTeam1Score')
      }
    ]
  };

  render() {
    return (
      <div>
        <h1>{this.props.stats.matchTitle}</h1>
        <h1>{`${this.props.stats.team1Name} ${this.props.stats.realTeam1Score} : ${this.props.stats.realTeam2Score} ${
          this.props.stats.team2Name
        }`}</h1>
        <Bar data={this.data} />
      </div>
    );
  }
}
