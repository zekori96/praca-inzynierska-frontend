import React from 'react';
import style from './GameTile.module.scss';
import moment from 'moment';
import { withRouter } from 'react-router-dom';
import { getOnlySetNumberOfFirstLetters } from '../../../services/TextService/TextService';

export default withRouter(props => {
  const isArchived = props.game.archived || moment.unix(props.game.endDate).isBefore(moment());
  return (
    <div
      className={[style.game, isArchived ? style.archived : null].join(' ')}
      onClick={() => props.history.push(`/game/${props.game.id}`)}
    >
      <img
        className={[style.image, isArchived ? style.archived : null].join(' ')}
        src={props.game.imageUrl}
        alt="game"
      />
      <div className={style.info}>
        <p className={style.header}>{getOnlySetNumberOfFirstLetters(props.game.title, 30)}</p>
        <p className={style.header}>{getOnlySetNumberOfFirstLetters(props.game.description, 30)}</p>
        <div>
          <p className={style.header}>{props.text.endDate}</p>
          <p className={style.value}>{moment.unix(props.game.endDate).format('DD.MM.YYYY HH:mm')}</p>
        </div>
        <div>
          <p className={style.header}>{props.text.created}</p>
          <p className={style.value}>{moment.unix(props.game.createdDate).format('DD.MM.YYYY HH:mm')}</p>
        </div>
        <div>
          <p className={style.header}>{props.text.creator}</p>
          <p className={style.value}>{props.game.creatorLogin}</p>
        </div>
        <div>
          <p className={style.header}>{props.text.userCount}</p>
          <p className={style.value}>{props.game.participants}</p>
        </div>
        <div>
          <p className={style.header}>{props.text.points}</p>
          <p className={style.value}>{props.game.points}</p>
        </div>
      </div>
    </div>
  );
});
