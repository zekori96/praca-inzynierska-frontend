import React from 'react';
import { endGame } from '../../../../services/GamePageService/GamePageService';
import EndModal from './EndModal';

jest.mock('../../../../services/GamePageService/GamePageService', () => ({
  endGame: jest.fn(() => Promise.resolve({ game: 'game' }))
}));
describe('EndModal', () => {
  let endModal;
  const props = {
    game: { id: 'id', matchDtos: [{ matchId: 'id' }] },
    refreshGameData: jest.fn(),
    onClose: jest.fn()
  };
  beforeEach(() => {
    endModal = new EndModal(props);
    endModal.setState = jest.fn();
  });

  describe('handleSave', () => {
    it('should call endGame with game id and matches stats then call refreshGameData with game and close modal', async () => {
      endModal.state = { matchId: { team1: 1 } };
      await endModal.handleSave();
      expect(endGame).toHaveBeenCalledWith('id', [{ id: 'matchId', team1: 1 }]);
      expect(props.refreshGameData).toHaveBeenCalledWith({ game: 'game' });
      expect(props.onClose).toHaveBeenCalled();
    });
  });

  describe('handleChange', () => {
    it('should add event to matchId object in state with identifier as key', async () => {
      await endModal.handleChange('event', 'matchId', 'identifier');
      expect(endModal.setState).toHaveBeenCalledWith({ matchId: { identifier: 'event' } });
    });
  });

  describe('checkIfDataIsCorrect', () => {
    it('should return true if checkIfNumberIsNotBlankAndPositive returns true for team1 and team2', () => {
      endModal.state = { matchId: { team1: 1, team2: 1 } };
      endModal.checkIfNumberIsNotBlankAndPositive = jest.fn(() => true);
      const result = endModal.checkIfDataIsCorrect();
      expect(result).toBe(true);
    });
    it('should return false if checkIfNumberIsNotBlankAndPositive returns false for team1', () => {
      endModal.state = { matchId: { team1: 'n', team2: 1 } };
      endModal.checkIfNumberIsNotBlankAndPositive = jest.fn(string => string !== 'n');
      const result = endModal.checkIfDataIsCorrect();
      expect(result).toBe(false);
    });
    it('should return false if checkIfStringIsNotBlank returns false for team2', () => {
      endModal.state = { matchId: { team1: 1, team2: 'n' } };
      endModal.checkIfNumberIsNotBlankAndPositive = jest.fn(string => string !== 'n');
      const result = endModal.checkIfDataIsCorrect();
      expect(result).toBe(false);
    });
    it('should return false if checkIfStringIsNotBlank returns false for team1 and team2', () => {
      endModal.state = { matchId: { team1: 'n', team2: 'n' } };
      endModal.checkIfNumberIsNotBlankAndPositive = jest.fn(string => string !== 'n');
      const result = endModal.checkIfDataIsCorrect();
      expect(result).toBe(false);
    });
  });
});
