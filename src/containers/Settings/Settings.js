import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { logIn, logOut } from '../../store/profile/profile.actions';
import {
  deleteProfile,
  getSettingsPage,
  sendEditedProfile
} from '../../services/SettingsPageService/SettingsPageService';
import InfoTile from '../../components/Settings/InfoTile/InfoTile';
import style from './Settings.module.scss';
import Modal from 'react-modal';
import Edit from '../../components/Settings/Edit/Edit';
import Delete from '../../components/Settings/Delete/Delete';
import Button from '../../components/shared/Button/Button';
import { connect } from 'react-redux';

export class Settings extends Component {
  state = {
    delete: { button: {} },
    settings: { text: { email: {}, login: {}, score: {} }, button: {} },
    modal: {
      isOpen: false,
      showEdit: false
    },
    errorMessage: []
  };

  componentDidMount() {
    getSettingsPage().then(state => this.setState({ ...this.state, ...state }));
  }

  updateProfile = (email, password, oldPassword) => {
    let editedData = { oldPassword: oldPassword, newPassword: password || undefined };
    if (this.props.email !== email) {
      editedData = { ...editedData, newEmail: email };
    }
    return sendEditedProfile(editedData).then(user => {
      this.props.logIn(user);
      this.closeModal();
    });
  };

  closeModal = () => {
    this.setState({
      modal: {
        ...this.state.modal,
        isOpen: false
      }
    });
  };

  handleDelete = () => {
    deleteProfile().then(() => {
      localStorage.setItem('auth', '');
      this.props.logOut();
      this.props.history.push('/');
    });
  };

  handleEditButtonClick = () => {
    this.setState({
      modal: {
        ...this.state.modal,
        showEdit: true,
        isOpen: true
      }
    });
  };
  handleDeleteButtonClick = () => {
    this.setState({
      modal: {
        ...this.state.modal,
        showEdit: false,
        isOpen: true
      }
    });
  };

  render() {
    return this.props.user ? (
      <div className={[style.content, this.state.modal.isOpen ? style['open-modal'] : null].join(' ')}>
        <Modal
          isOpen={this.state.modal.isOpen}
          onRequestClose={this.closeModal}
          className={style.modal}
          contentLabel="Settings Modal"
        >
          {this.state.modal.showEdit ? (
            <Edit
              close={this.closeModal}
              text={{
                passwordLengthErrorMessage: this.state.edit.error.passwordLengthErrorMessage,
                emailErrorMessage: this.state.edit.error.emailErrorMessage,
                serverErrorMessage: this.state.edit.error.serverErrorMessage,
                description: this.state.edit.text.info,
                buttonAccept: this.state.edit.button,
                newPassword: this.state.edit.text.newPassword,
                oldPassword: this.state.edit.text.oldPassword
              }}
              updateProfile={this.updateProfile}
              email={this.props.user.email}
            />
          ) : (
            <Delete
              close={this.closeModal}
              warning={this.state.delete.icon}
              details={this.state.delete.text}
              delete={this.handleDelete}
              buttonYes={this.state.delete.button.yes}
              buttonNo={this.state.delete.button.no}
            />
          )}
        </Modal>
        <div className={style.tiles}>
          <InfoTile
            icon={this.state.settings.text.email.icon}
            alt={'email'}
            value={this.props.user.email}
            description={this.state.settings.text.email.text}
          />
          <InfoTile
            icon={this.state.settings.text.login.icon}
            alt={'login'}
            value={this.props.user.login}
            description={this.state.settings.text.login.text}
          />
          <InfoTile
            icon={this.state.settings.text.score.icon}
            alt={'score'}
            value={this.props.user.score}
            description={this.state.settings.text.score.text}
          />
        </div>
        <div className={style.buttons}>
          <Button onClick={this.handleEditButtonClick} text={this.state.settings.button.edit} />
          <Button onClick={this.handleDeleteButtonClick} text={this.state.settings.button.delete} />
        </div>
      </div>
    ) : null;
  }
}

function mapStateToProps(state) {
  const user = state.profile.user;

  return {
    user
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    { logIn, logOut }
  )(Settings)
);
