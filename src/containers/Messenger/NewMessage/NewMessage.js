import React, { Component } from 'react';
import style from './NewMessage.module.scss';
import Input from '../../../components/shared/Input/Input';
import UserTile from '../../../components/Messenger/NewMessage/UserTile/UserTile';
import {
  getConversation,
  getNewMessagePage,
  getUsers,
  sendInvite
} from '../../../services/NewMessageService/NewMessageService';
import BackButton from '../../../components/shared/BackButton/BackButton';
import qs from 'qs';

export class NewMessage extends Component {
  state = {
    search: '',
    users: [],
    gameId: undefined
  };

  componentDidMount() {
    return Promise.all([getNewMessagePage(), getUsers()]).then(results => {
      this.setState({
        ...this.state,
        gameId: qs.parse(this.props.location.search, { ignoreQueryPrefix: true }).gameId,
        users: results[1],
        ...results[0]
      });
    });
  }

  handleUserClick = userId => {
    if (this.state.gameId) {
      sendInvite(userId, this.state.gameId).then(conversationId =>
        this.props.history.push(`/conversation/${conversationId}`)
      );
    } else {
      getConversation(userId).then(conversationId => this.props.history.push(`/conversation/${conversationId}`));
    }
  };

  handleSearch = event => {
    getUsers(event.target.value).then(users => this.setState({ users: users }));
  };

  render() {
    return (
      <div className={style.container}>
        <div className={style['back-button-container']}>
          <BackButton onClick={this.props.history.goBack} />
        </div>

        <Input className={style.search} placeholder={this.state.search} changed={this.handleSearch} />
        <div className={style.users}>
          {this.state.users.map((user, index) => (
            <UserTile key={index} userId={user.userId} login={user.userLogin} onClick={this.handleUserClick} />
          ))}
        </div>
      </div>
    );
  }
}
