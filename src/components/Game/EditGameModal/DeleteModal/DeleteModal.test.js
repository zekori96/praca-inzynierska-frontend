import React from 'react';
import { DeleteModal } from './DeleteModal';
import { deleteGame } from '../../../../services/GamePageService/GamePageService';

jest.mock('../../../../services/GamePageService/GamePageService', () => ({
  deleteGame: jest.fn(() => Promise.resolve({}))
}));
describe('DeleteModal', () => {
  let deleteModal;
  const props = { game: { id: 'id' }, history: { push: jest.fn() } };
  beforeEach(() => {
    deleteModal = new DeleteModal(props);
  });

  describe('handleClick', () => {
    it('should call deleteGame with game id then redirect to home page', async () => {
      await deleteModal.handleClick();
      expect(deleteGame).toHaveBeenCalledWith('id');
      expect(props.history.push).toHaveBeenCalledWith('/');
    });
  });
});
