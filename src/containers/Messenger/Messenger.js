import React, { Component } from 'react';
import style from './Messenger.module.scss';
import { getConversations, getMessengerPage } from '../../services/MessengerPageService/MessengerPageService';
import Input from '../../components/shared/Input/Input';
import ConversationTile from '../../components/Messenger/ConversationTile/ConversationTile';
import { withRouter } from 'react-router-dom';
import Button from '../../components/shared/Button/Button';
import { getOnlySetNumberOfFirstLetters } from '../../services/TextService/TextService';
import { connect } from 'react-redux';

export class Messenger extends Component {
  state = {
    conversations: []
  };

  componentDidMount() {
    return Promise.all([getMessengerPage(), getConversations()]).then(results => {
      this.setState({ ...results[0], conversations: results[1] });
    });
  }

  handleSearch = event => {
    return getConversations(event.target.value).then(conversations =>
      this.setState({ searchValue: event.target.value, conversations: conversations })
    );
  };

  handleConversationClick = identifier => {
    this.props.history.push(`/conversation/${identifier}`);
  };

  render() {
    return (
      <div className={style.container}>
        <Button onClick={() => this.props.history.push('/conversation/new')} text={this.state.newMessage} />
        <Input
          className={style.search}
          placeholder={this.state.search}
          value={this.state.searchValue}
          changed={this.handleSearch}
        />
        {this.state.conversations.map((conversation, index) => (
          <ConversationTile
            key={index}
            id={conversation.id}
            userId={conversation.user.id}
            login={conversation.user.login}
            message={getOnlySetNumberOfFirstLetters(conversation.lastMessage.text || this.state.invitation, 30)}
            date={conversation.lastMessage.date}
            isNew={conversation.received && conversation.lastMessage.new}
            onClick={this.handleConversationClick}
          />
        ))}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const user = state.profile.user;

  return {
    user
  };
}

export default withRouter(connect(mapStateToProps)(Messenger));
