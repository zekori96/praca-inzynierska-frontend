import React from 'react';
import { Messenger } from './Messenger';
import { getConversations, getMessengerPage } from '../../services/MessengerPageService/MessengerPageService';

jest.mock('../../services/MessengerPageService/MessengerPageService', () => ({
  getMessengerPage: jest.fn(() => Promise.resolve({ text: 'text' })),
  getConversations: jest.fn(() => Promise.resolve([{ id: 'id' }]))
}));
describe('Messenger', () => {
  let messenger;
  const props = {
    isLoggedIn: true,
    logIn: jest.fn(),
    history: { push: jest.fn() },
    location: { pathname: 'xxx/yyy/zzz?gameId=id' }
  };
  beforeEach(() => {
    messenger = new Messenger(props);
    messenger.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should call getMessengerPage and getConversations then add results to state', async () => {
      await messenger.componentDidMount();
      expect(getConversations).toHaveBeenCalled();
      expect(getMessengerPage).toHaveBeenCalled();
      expect(messenger.setState).toHaveBeenCalledWith({ text: 'text', conversations: [{ id: 'id' }] });
    });
  });

  describe('handleSearch', () => {
    it('should call getConversations with event value then add result to state with searchValue', async () => {
      await messenger.handleSearch({ target: { value: 'value' } });
      expect(getConversations).toHaveBeenCalledWith('value');
      expect(messenger.setState).toHaveBeenCalledWith({ searchValue: 'value', conversations: [{ id: 'id' }] });
    });
  });

  describe('handleConversationClick', () => {
    it('should redirect to conversation with identifier', async () => {
      await messenger.handleConversationClick('identifier');
      expect(props.history.push).toHaveBeenCalledWith('/conversation/identifier');
    });
  });
});
