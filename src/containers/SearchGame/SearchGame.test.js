import React from 'react';
import { SearchGame } from './SearchGame';
import { getGames, getSearchGamePage } from '../../services/SearchGamePageService/SearchGamePageService';

jest.mock('../../services/SearchGamePageService/SearchGamePageService', () => ({
  getSearchGamePage: jest.fn(() => Promise.resolve({ text: 'text' })),
  getGames: jest.fn(() => Promise.resolve([{ id: 'id' }]))
}));
describe('SearchGame', () => {
  let searchGame;
  const props = {};
  beforeEach(() => {
    searchGame = new SearchGame(props);
    searchGame.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should call getSearchGamePage then add result to state and update games', async () => {
      searchGame.updateGames = jest.fn();
      searchGame.state = { searchBox: 'searchBox', sortId: 'sortId', sortTypeId: 'sortTypeId', filterId: 'filterId' };
      await searchGame.componentDidMount();
      expect(getSearchGamePage).toHaveBeenCalled();
      expect(searchGame.setState).toHaveBeenCalledWith({ text: 'text' });
      expect(searchGame.updateGames).toHaveBeenCalledWith('searchBox', 'sortId', 'filterId', 'sortTypeId');
    });
  });
  describe('handleSearchBox', () => {
    it('should add event value to state and update games', async () => {
      searchGame.updateGames = jest.fn();
      searchGame.state = { sortId: 'sortId', sortTypeId: 'sortTypeId', filterId: 'filterId' };
      await searchGame.handleSearchBox({ target: { value: 'value' } });
      expect(searchGame.setState).toHaveBeenCalledWith({ searchBox: 'value' });
      expect(searchGame.updateGames).toHaveBeenCalledWith('value', 'sortId', 'filterId', 'sortTypeId');
    });
  });
  describe('handleSortButtonClick', () => {
    it('should add event value to state and update games', async () => {
      searchGame.updateGames = jest.fn();
      searchGame.state = { searchBox: 'searchBox', sortTypeId: 'sortTypeId', filterId: 'filterId' };
      await searchGame.handleSortButtonClick('value');
      expect(searchGame.setState).toHaveBeenCalledWith({ sortId: 'value' });
      expect(searchGame.updateGames).toHaveBeenCalledWith('searchBox', 'value', 'filterId', 'sortTypeId');
    });
  });

  describe('handleSortType', () => {
    it('should add event value to state and update games', async () => {
      searchGame.updateGames = jest.fn();
      searchGame.state = { searchBox: 'searchBox', sortId: 'sortId', filterId: 'filterId' };
      await searchGame.handleSortType('value');
      expect(searchGame.setState).toHaveBeenCalledWith({ sortTypeId: 'value' });
      expect(searchGame.updateGames).toHaveBeenCalledWith('searchBox', 'sortId', 'filterId', 'value');
    });
  });

  describe('handleFilterButtonClick', () => {
    it('should add event value to state and update games', async () => {
      searchGame.updateGames = jest.fn();
      searchGame.state = { searchBox: 'searchBox', sortId: 'sortId', sortTypeId: 'sortTypeId' };
      await searchGame.handleFilterButtonClick('value');
      expect(searchGame.setState).toHaveBeenCalledWith({ filterId: 'value' });
      expect(searchGame.updateGames).toHaveBeenCalledWith('searchBox', 'sortId', 'value', 'sortTypeId');
    });
  });

  describe('updateGames', () => {
    it('should get games then add result to state', async () => {
      await searchGame.updateGames('searchBox', 'sortId', 'sortTypeId', 'filterId');
      expect(getGames).toHaveBeenCalledWith('searchBox', 'sortId', 'sortTypeId', 'filterId');
      expect(searchGame.setState).toHaveBeenCalledWith({ games: [{ id: 'id' }] });
    });
  });
});
