import React from 'react';
import { LogIn } from './LogIn';
import { getLogInPage, sendLogInRequest } from '../../services/LogInPageService/LogInPageService';

jest.mock('../../services/LogInPageService/LogInPageService', () => ({
  getLogInPage: jest.fn(() => Promise.resolve({ text: 'text' })),
  sendLogInRequest: jest.fn(() => Promise.resolve({ profile: 'profile' }))
}));
describe('LogIn', () => {
  let logIn;
  const props = {
    isLoggedIn: true,
    logIn: jest.fn(),
    history: { push: jest.fn() }
  };
  beforeEach(() => {
    logIn = new LogIn(props);
    logIn.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should call getLogInPage then add result to state', async () => {
      await logIn.componentDidMount();
      expect(getLogInPage).toHaveBeenCalled();
      expect(logIn.setState).toHaveBeenCalledWith({ text: 'text' });
    });
  });

  describe('inputChangedHandler', () => {
    it('should add event value to logInForm in state with input as key', async () => {
      await logIn.inputChangedHandler({ target: { value: 'value' } }, 'identifier');
      expect(logIn.setState).toHaveBeenCalledWith({
        logInForm: {
          identifier: 'value',
          login: '',
          password: ''
        }
      });
    });
  });
  describe('logInHandler', () => {
    it('should prevent default behaviour then sendLogInRequest if form is correct', async () => {
      const event = { preventDefault: jest.fn() };
      logIn.state = { logInForm: { login: 'login', password: 'password' } };
      logIn.checkIfFormIsCorrect = jest.fn(() => true);
      await logIn.logInHandler(event);
      expect(event.preventDefault).toHaveBeenCalled();
      expect(logIn.checkIfFormIsCorrect).toHaveBeenCalled();
      expect(sendLogInRequest).toHaveBeenCalledWith('login', 'password');
      expect(props.logIn).toHaveBeenCalledWith({ profile: 'profile' });
      expect(props.history.push).toHaveBeenCalledWith('/');
    });
    it('should prevent default behaviour then show wrong data error message if form is not correct', async () => {
      const event = { preventDefault: jest.fn() };
      logIn.state = { logInForm: { login: 'login', password: 'password' } };
      logIn.checkIfFormIsCorrect = jest.fn(() => false);
      logIn.showWrongDataErrorMessage = jest.fn();
      await logIn.logInHandler(event);
      expect(event.preventDefault).toHaveBeenCalled();
      expect(logIn.checkIfFormIsCorrect).toHaveBeenCalled();
      expect(logIn.showWrongDataErrorMessage).toHaveBeenCalled();
    });
    it('should prevent default behaviour then sendLogInRequest if form is correct then show wrong data error message if sendLogInRequest fail', async () => {
      const event = { preventDefault: jest.fn() };
      logIn.state = { logInForm: { login: 'login', password: 'password' } };
      logIn.checkIfFormIsCorrect = jest.fn(() => true);
      logIn.showWrongDataErrorMessage = jest.fn();
      sendLogInRequest.mockReturnValue(Promise.reject());
      jest.spyOn(global.console, 'error').mockImplementation(() => {});
      await logIn.logInHandler(event);
      expect(event.preventDefault).toHaveBeenCalled();
      expect(logIn.checkIfFormIsCorrect).toHaveBeenCalled();
      expect(sendLogInRequest).toHaveBeenCalledWith('login', 'password');
      expect(logIn.showWrongDataErrorMessage).toHaveBeenCalled();
    });
  });

  describe('checkIfFormIsCorrect', () => {
    it('should return true if login and password are not empty', () => {
      logIn.state = { logInForm: { login: 'login', password: 'password' } };
      expect(logIn.checkIfFormIsCorrect()).toBe(true);
    });
    it('should return false if login is empty', () => {
      logIn.state = { logInForm: { login: '', password: 'password' } };
      expect(logIn.checkIfFormIsCorrect()).toBe(false);
    });
    it('should return false if password is empty', () => {
      logIn.state = { logInForm: { login: 'login', password: '' } };
      expect(logIn.checkIfFormIsCorrect()).toBe(false);
    });
  });
  describe('showWrongDataErrorMessage', () => {
    it('should add error message to state then reset form', () => {
      logIn.state = { wrongDataErrorMessage: 'wrongDataErrorMessage' };
      logIn.form = { reset: jest.fn() };
      logIn.showWrongDataErrorMessage();
      expect(logIn.setState).toHaveBeenCalledWith({ errorMessage: 'wrongDataErrorMessage' });
      expect(logIn.form.reset).toHaveBeenCalled();
    });
  });
});
