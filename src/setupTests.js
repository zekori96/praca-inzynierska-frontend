const localStorageMock = {
  getItem: jest.fn(),
  setItem: jest.fn(),
  removeItem: jest.fn(),
  clear: jest.fn()
};
const window = {
  scrollTo: jest.fn(),
  addEventListener: jest.fn()
};
global.localStorage = localStorageMock;
global.window = window;
