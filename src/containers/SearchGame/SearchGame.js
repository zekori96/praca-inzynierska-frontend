import React, { Component } from 'react';
import style from './SearchGame.module.scss';
import { getGames, getSearchGamePage } from '../../services/SearchGamePageService/SearchGamePageService';
import RadioBoxes from '../../components/SearchGame/RadioBoxes/RadioBoxes';
import Input from '../../components/shared/Input/Input';
import GameTile from '../../components/SearchGame/GameTile/GameTile';

export class SearchGame extends Component {
  state = {
    text: {
      sort: {},
      filter: {},
      search: {}
    },
    games: [],
    searchBox: '',
    sortTypeId: 'descending',
    sortId: 'endDate',
    filterId: 'title',
    sortBoxes: [],
    filterBoxes: [],
    sortTypeBoxes: []
  };

  componentDidMount() {
    getSearchGamePage().then(text => this.setState(text));
    const { searchBox, sortId, sortTypeId, filterId } = this.state;
    this.updateGames(searchBox, sortId, filterId, sortTypeId);
  }

  handleSearchBox = event => {
    this.setState({ searchBox: event.target.value });
    const { sortId, sortTypeId, filterId } = this.state;
    this.updateGames(event.target.value, sortId, filterId, sortTypeId);
  };

  handleSortButtonClick = sortId => {
    this.setState({ sortId: sortId });
    const { searchBox, sortTypeId, filterId } = this.state;
    this.updateGames(searchBox, sortId, filterId, sortTypeId);
  };

  handleSortType = sortTypeId => {
    this.setState({ sortTypeId: sortTypeId });
    const { searchBox, sortId, filterId } = this.state;
    this.updateGames(searchBox, sortId, filterId, sortTypeId);
  };

  handleFilterButtonClick = filterId => {
    this.setState({ filterId: filterId });
    const { searchBox, sortTypeId, sortId } = this.state;
    this.updateGames(searchBox, sortId, filterId, sortTypeId);
  };

  updateGames(searchValue, sortId, filterId, sortTypeId) {
    getGames(searchValue, sortId, filterId, sortTypeId).then(games => this.setState({ games: games }));
  }

  render() {
    return (
      <div className={style.container}>
        <div className={style.search}>
          <Input
            type="text"
            changed={this.handleSearchBox}
            placeholder={this.state.text.search.placeholder}
            value={this.state.searchBox}
          />
          <div className={style['search-buttons']}>
            <RadioBoxes
              title={this.state.text.sort.text}
              handle={this.handleSortButtonClick}
              boxes={this.state.sortBoxes}
              currentValue={this.state.sortId}
            />
            <RadioBoxes
              title={this.state.text.filter.text}
              handle={this.handleFilterButtonClick}
              boxes={this.state.filterBoxes}
              currentValue={this.state.filterId}
            />
            <RadioBoxes
              title={this.state.text.order}
              handle={this.handleSortType}
              boxes={this.state.sortTypeBoxes}
              currentValue={this.state.sortTypeId}
            />
          </div>
        </div>

        <div className={style.games}>
          {this.state.games.map((game, index) => (
            <GameTile game={game} key={index} text={this.state.text} />
          ))}
        </div>
      </div>
    );
  }
}
