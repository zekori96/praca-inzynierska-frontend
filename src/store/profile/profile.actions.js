export const LOGIN = 'LOGIN';

export function logIn(user) {
  return {
    type: LOGIN,
    payload: user
  };
}

export const LOGOUT = 'LOGOUT';

export function logOut() {
  return {
    type: LOGOUT
  };
}
