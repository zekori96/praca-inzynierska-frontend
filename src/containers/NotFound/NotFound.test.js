import React from 'react';
import { NotFound } from './NotFound';
import { getNotFoundPage } from '../../services/NotFoundPageService/NotFoundPageService';

jest.mock('../../services/NotFoundPageService/NotFoundPageService', () => ({
  getNotFoundPage: jest.fn(() => Promise.resolve({ text: 'text' })),
  getConversations: jest.fn(() => Promise.resolve([{ id: 'id' }]))
}));
describe('NotFound', () => {
  let notFound;
  const props = {};
  beforeEach(() => {
    notFound = new NotFound(props);
    notFound.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should call getNotFoundPage then add result to state', async () => {
      await notFound.componentDidMount();
      expect(getNotFoundPage).toHaveBeenCalled();
      expect(notFound.setState).toHaveBeenCalledWith({ text: 'text' });
    });
  });
});
