import React, { Component } from 'react';
import style from './LandingPage.module.scss';
import { getLandingPage } from '../../services/LandingPageService/LandingPageService';
import { getAdditionalTexts } from '../../services/contentfulClient';
import { withRouter } from 'react-router-dom';
import Markdown from 'markdown-to-jsx';
import Button from '../../components/shared/Button/Button';
import { connect } from 'react-redux';

export class LandingPage extends Component {
  state = {
    content: {
      info: [],
      bulletPoints: [],
      welcomeImages: []
    },
    logInButtonText: ''
  };

  componentDidMount() {
    getLandingPage().then(state => this.setState(state));
    getAdditionalTexts().then(texts => this.setState({ logInButtonText: texts.logIn }));
  }

  getRandomWelcomeImage() {
    if (this.state.content.welcomeImages.length > 0) {
      return this.state.content.welcomeImages[Math.floor(Math.random() * this.state.content.welcomeImages.length)].url;
    }
    return '';
  }

  render() {
    return (
      <div className={style.container}>
        <div className={style['image-container']}>
          <img src={this.getRandomWelcomeImage()} alt="welcome" />
          {!this.props.isLoggedIn ? (
            <Button onClick={() => this.props.history.push('/logIn')} text={this.state.logInButtonText} />
          ) : null}
        </div>
        {this.state.content.info.map((info, index) => (
          <div key={index} className={style['info-container']}>
            <h1>{info.header}</h1>
            <Markdown>{info.content}</Markdown>
          </div>
        ))}
        {this.state.content.bulletPoints.map((bulletPoint, index) => (
          <div key={index} className={style['bullet-point-container']}>
            <img src={bulletPoint.icon.url} alt="bulletPoint" />
            <h2>{bulletPoint.header}</h2>
            <p>{bulletPoint.content}</p>
          </div>
        ))}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const isLoggedIn = !!state.profile.user;

  return {
    isLoggedIn
  };
}

export default withRouter(connect(mapStateToProps)(withRouter(LandingPage)));
