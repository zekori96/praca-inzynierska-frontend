import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import ScrollToTop from './containers/ScrollToTop/ScrollToTop';
import { Redirect, Route, Switch } from 'react-router-dom';
import LandingPage from './containers/LandingPage/LandingPage';
import LogIn from './containers/LogIn/LogIn';
import Register from './containers/Register/Register';
import Header from './containers/Header/Header';
import { createBrowserHistory } from 'history';
import { Ranking } from './containers/Ranking/Ranking';
import LogOut from './components/LogOut/LogOut';
import { getStore } from './store';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import Settings from './containers/Settings/Settings';
import { NotFound } from './containers/NotFound/NotFound';
import Messenger from './containers/Messenger/Messenger';
import { NewMessage } from './containers/Messenger/NewMessage/NewMessage';
import Conversation from './containers/Messenger/Conversation/Conversation';
import { SearchGame } from './containers/SearchGame/SearchGame';
import { Game } from './containers/Game/Game';
import { NewGame } from './containers/NewGame/NewGame';

const history = createBrowserHistory();
const store = getStore(history);
ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <ScrollToTop>
        <Header>
          <main>
            <Switch>
              <Route exact path={'/'} component={LandingPage} />
              <Route exact path={'/logIn'} component={LogIn} />
              <Route exact path={'/logOut'} component={LogOut} />
              <Route exact path={'/register'} component={Register} />
              <Route exact path={'/ranking'} component={Ranking} />
              <Route exact path={'/settings'} component={Settings} />
              <Route exact path={'/conversation'} component={Messenger} />
              <Route exact path={'/conversation/new'} component={NewMessage} />
              <Route exact path={'/conversation/:id'} component={Conversation} />
              <Route exact path={'/games'} component={SearchGame} />
              <Route exact path={'/games/create'} component={NewGame} />
              <Route exact path={'/game/:id'} component={Game} />
              <Route exact path={'/404'} component={NotFound} />
              <Redirect to={`/404`} />
            </Switch>
          </main>
        </Header>
      </ScrollToTop>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);
