import React from 'react';
import Edit from './Edit';
import { checkIfStringIsNotBlank } from '../../../services/ValidationService/ValidationService';

jest.mock('../../../services/ValidationService/ValidationService', () => ({
  checkIfStringIsNotBlank: jest.fn(string => string !== ''),
  validateEmail: jest.fn(string => string !== 'email')
}));
describe('Edit', () => {
  let edit;
  const props = {
    email: 'email',
    text: { newPassword: 'newPassword', oldPassword: 'oldPassword' },
    updateProfile: jest.fn(() => Promise.resolve())
  };
  beforeEach(() => {
    edit = new Edit(props);
    edit.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should add email, new password and old password from props to state', async () => {
      await edit.componentDidMount();
      expect(edit.setState).toHaveBeenCalledWith({
        email: 'email',
        password: 'newPassword',
        oldPassword: 'oldPassword'
      });
    });
  });

  describe('onEmailChange', () => {
    it('should set event value to state as email', async () => {
      await edit.onEmailChange({ target: { value: 'value' } });
      expect(edit.setState).toHaveBeenCalledWith({ email: 'value' });
    });
  });

  describe('onPasswordChange', () => {
    it('should set event value to state as password', async () => {
      await edit.onPasswordChange({ target: { value: 'value' } });
      expect(edit.setState).toHaveBeenCalledWith({ password: 'value' });
    });
  });

  describe('onOldPasswordChange', () => {
    it('should set event value to state as oldPassword', async () => {
      await edit.onOldPasswordChange({ target: { value: 'value' } });
      expect(edit.setState).toHaveBeenCalledWith({ oldPassword: 'value' });
    });
  });

  describe('checkIfSaveDataAreCorrect', () => {
    it('should return true if all data is correct', async () => {
      edit.state = { email: 'email@email', password: 'password', oldPassword: 'oldPassword' };
      const result = await edit.checkIfSaveDataAreCorrect();
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('email@email');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('password');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('oldPassword');
      expect(result).toEqual({ isCorrect: true, type: [] });
    });
    it('should return false with type 3 if email is blank', async () => {
      edit.state = { email: '', password: 'password', oldPassword: 'oldPassword' };
      const result = await edit.checkIfSaveDataAreCorrect();
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('password');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('oldPassword');
      expect(result).toEqual({ isCorrect: false, type: [3] });
    });
    it('should return false with type 3 if password is blank', async () => {
      edit.state = { email: 'email@email', password: '', oldPassword: 'oldPassword' };
      const result = await edit.checkIfSaveDataAreCorrect();
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('email@email');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('oldPassword');
      expect(result).toEqual({ isCorrect: false, type: [3, 1] });
    });
    it('should return false with type 3 if oldPassword is blank', async () => {
      edit.state = { email: 'email@email', password: 'password', oldPassword: '' };
      const result = await edit.checkIfSaveDataAreCorrect();
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('email@email');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('password');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('');
      expect(result).toEqual({ isCorrect: false, type: [3] });
    });
    it('should return false with type 1 if password has less than 8 characters', async () => {
      edit.state = { email: 'email@email', password: 'pass', oldPassword: 'oldPassword' };
      const result = await edit.checkIfSaveDataAreCorrect();
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('email@email');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('pass');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('oldPassword');
      expect(result).toEqual({ isCorrect: false, type: [1] });
    });
    it('should return false with type 2 if email is not correct', async () => {
      edit.state = { email: 'email', password: 'password', oldPassword: 'oldPassword' };
      const result = await edit.checkIfSaveDataAreCorrect();
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('email');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('password');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('oldPassword');
      expect(result).toEqual({ isCorrect: false, type: [2] });
    });
  });

  describe('appendPasswordLengthError', () => {
    it('should add error message to state', async () => {
      await edit.appendPasswordLengthError();
      expect(edit.setState).toHaveBeenCalledWith({ errorMessage: [<p />] });
    });
  });

  describe('appendEmailError', () => {
    it('should add error message to state', async () => {
      await edit.appendEmailError();
      expect(edit.setState).toHaveBeenCalledWith({ errorMessage: [<p />] });
    });
  });

  describe('appendServerError', () => {
    it('should add error message to state', async () => {
      await edit.appendServerError();
      expect(edit.setState).toHaveBeenCalledWith({ errorMessage: [<p />] });
    });
  });

  describe('handleSave', () => {
    it('should clear old error messages then update profile if data is correct', async () => {
      edit.state = { email: 'email', password: 'password', oldPassword: 'oldPassword' };
      edit.checkIfSaveDataAreCorrect = jest.fn(() => ({ isCorrect: true, type: [] }));
      await edit.handleSave();
      expect(edit.setState).toHaveBeenCalledWith({ errorMessage: [] });
      expect(edit.checkIfSaveDataAreCorrect).toHaveBeenCalled();
      expect(props.updateProfile).toHaveBeenCalledWith('email', 'password', 'oldPassword');
    });
    it('should clear old error messages then call appendServerError if data is correct but updateProfile return reject', async () => {
      edit.state = { email: 'email', password: 'password', oldPassword: 'oldPassword' };
      edit.checkIfSaveDataAreCorrect = jest.fn(() => ({ isCorrect: true, type: [] }));
      edit.props.updateProfile = jest.fn(() => Promise.reject());
      edit.appendServerError = jest.fn();
      jest.spyOn(global.console, 'error').mockImplementation(() => {});
      await edit.handleSave();
      expect(edit.setState).toHaveBeenCalledWith({ errorMessage: [] });
      expect(edit.checkIfSaveDataAreCorrect).toHaveBeenCalled();
      expect(props.updateProfile).toHaveBeenCalledWith('email', 'password', 'oldPassword');
      expect(edit.appendServerError).toHaveBeenCalled();
    });
    it('should clear old error messages then call appendPasswordLengthError if data is not correct and type is 1', async () => {
      edit.checkIfSaveDataAreCorrect = jest.fn(() => ({ isCorrect: false, type: [1] }));
      edit.appendPasswordLengthError = jest.fn();
      await edit.handleSave();
      expect(edit.setState).toHaveBeenCalledWith({ errorMessage: [] });
      expect(edit.checkIfSaveDataAreCorrect).toHaveBeenCalled();
      expect(edit.appendPasswordLengthError).toHaveBeenCalled();
    });
    it('should clear old error messages then call appendEmailError if data is not correct and type is 2', async () => {
      edit.checkIfSaveDataAreCorrect = jest.fn(() => ({ isCorrect: false, type: [2] }));
      edit.appendEmailError = jest.fn();
      await edit.handleSave();
      expect(edit.setState).toHaveBeenCalledWith({ errorMessage: [] });
      expect(edit.checkIfSaveDataAreCorrect).toHaveBeenCalled();
      expect(edit.appendEmailError).toHaveBeenCalled();
    });
  });
});
