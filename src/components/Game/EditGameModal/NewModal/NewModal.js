import React, { Component } from 'react';
import style from './NewModal.module.scss';
import Input from '../../../shared/Input/Input';
import Button from '../../../shared/Button/Button';
import BackButton from '../../../shared/BackButton/BackButton';
import DateTimeSelector from '../../../shared/DateTimeSelector/DateTimeSelector';
import { addMatchToGame } from '../../../../services/GamePageService/GamePageService';
import * as moment from 'moment';
import {
  checkIfDateIsCorrect,
  checkIfStringIsNotBlank
} from '../../../../services/ValidationService/ValidationService';

export default class NewModal extends Component {
  state = {
    newMatch: {},
    errorMessage: []
  };
  handleNewTypingChange = (event, identifier) => {
    this.setState({
      newMatch: { ...this.state.newMatch, [identifier]: event }
    });
  };
  handleNewTypingSave = () => {
    addMatchToGame(this.props.game.id, this.state.newMatch).then(game => {
      this.props.refreshGameData(game);
      this.props.onClose();
    });
  };

  checkIfMatchIsCorrect() {
    let areMatchesDataCorrect = true;
    const { date, team1Name, team2Name, name } = this.state.newMatch;
    areMatchesDataCorrect =
      areMatchesDataCorrect &&
      checkIfDateIsCorrect(date) &&
      checkIfStringIsNotBlank(team1Name) &&
      checkIfStringIsNotBlank(team2Name) &&
      checkIfStringIsNotBlank(name);
    return areMatchesDataCorrect;
  }

  render() {
    console.log(this.props);
    return (
      <div className={style.container}>
        <BackButton className={style['back-button']} onClick={this.props.onClose} />
        {this.state.errorMessage ? (
          <div className={style.errorMessage}>{this.state.errorMessage.map(err => err.value)}</div>
        ) : null}
        <div>
          <Input
            placeholder={this.props.text.placeholder.playoffName}
            changed={event => this.handleNewTypingChange(event.target.value, 'name')}
          />
        </div>
        <div>
          <Input
            placeholder={this.props.text.placeholder.team1}
            changed={event => this.handleNewTypingChange(event.target.value, 'team1Name')}
          />
        </div>
        <div>
          <Input
            placeholder={this.props.text.placeholder.team2}
            changed={event => this.handleNewTypingChange(event.target.value, 'team2Name')}
          />
        </div>
        <div>
          <DateTimeSelector
            onChange={this.handleNewTypingChange}
            date={this.props.text.placeholder.date}
            identifier={'date'}
            enableAfter={moment()}
            enableBefore={moment.unix(this.props.game.endDate)}
          />
        </div>
        <div>
          <Button
            onClick={this.handleNewTypingSave}
            disabled={!this.checkIfMatchIsCorrect()}
            text={this.props.text.button.acceptChanges}
          />
        </div>
      </div>
    );
  }
}
