import React, { Component } from 'react';
import style from './EditModal.module.scss';
import Input from '../../../shared/Input/Input';
import Button from '../../../shared/Button/Button';
import BackButton from '../../../shared/BackButton/BackButton';
import { updateGameInfo } from '../../../../services/GamePageService/GamePageService';
import { checkIfStringIsNotBlank } from '../../../../services/ValidationService/ValidationService';

export default class EditModal extends Component {
  state = {};

  componentDidMount() {
    this.setState({ title: this.props.game.title, description: this.props.game.description });
  }

  handleChange = (event, identifier) => {
    this.setState({ [identifier]: event });
  };
  handleSave = () => {
    updateGameInfo(this.props.game.id, { title: this.state.title, description: this.state.description }).then(game => {
      this.props.refreshGameData(game);
      this.props.onClose();
    });
  };

  checkIfDataIsCorrect() {
    let dataIsCorrect = true;
    const { title, description } = this.state;
    dataIsCorrect = dataIsCorrect && checkIfStringIsNotBlank(title) && checkIfStringIsNotBlank(description);
    return dataIsCorrect;
  }

  render() {
    return (
      <div className={style.container}>
        <BackButton className={style['back-button']} onClick={this.props.onClose} />
        <div>
          <Input value={this.state.title || ''} changed={event => this.handleChange(event.target.value, 'title')} />
        </div>
        <div>
          <Input
            value={this.state.description || ''}
            changed={event => this.handleChange(event.target.value, 'description')}
          />
        </div>
        <div>
          <Button
            onClick={this.handleSave}
            disabled={!this.checkIfDataIsCorrect()}
            text={this.props.acceptChangesText}
          />
        </div>
      </div>
    );
  }
}
