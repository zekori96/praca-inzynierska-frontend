import { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { isUserLoggedIn } from '../../services/LogInPageService/LogInPageService';
import { connect } from 'react-redux';
import { logIn, logOut } from '../../store/profile/profile.actions';
import { instance } from '../../services/AxiosService/AxiosService';

export class ScrollToTop extends Component {
  componentDidMount() {
    if (isUserLoggedIn()) {
      instance.get('/user').then(response => this.props.logIn(response.data));
    } else {
      this.props.logOut();
      localStorage.removeItem('auth');
      this.props.history.push('/');
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    return this.props.children;
  }
}

function mapStateToProps(state) {
  const isLoggedIn = !!state.profile.user;

  return {
    isLoggedIn
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    { logIn, logOut }
  )(ScrollToTop)
);
