import React, { Component, Fragment } from 'react';
import style from './Game.module.scss';
import BackButton from '../../components/shared/BackButton/BackButton';
import { getGame, getGamePage, getStats, signOut } from '../../services/GamePageService/GamePageService';
import Modal from 'react-modal';
import TypingsShower from '../../components/Game/TypingsShower/TypingsShower';
import GameButtons from '../../components/Game/GameButtons/GameButtons';
import ModalSelector from '../../components/Game/ModalSelector/ModalSelector';
import InfoContainer from '../../components/Game/InfoContainer/InfoContainer';
import moment from 'moment';
import { Stats } from '../../components/Game/Stats/Stats';

Modal.setAppElement('#root');

export class Game extends Component {
  state = {
    game: { matchDtos: [] },
    text: { button: {}, editModal: {}, deleteModal: {}, endModal: {} },
    typedGames: [],
    userType: () => {
      if (this.state.game.creator) {
        return 'creator';
      }
      if (this.state.game.participating) {
        return 'participant';
      }
      return 'new';
    },
    modal: {
      isOpen: false,
      type: ''
    }
  };

  componentDidMount() {
    const gameId = this.props.location.pathname.substr(this.props.location.pathname.lastIndexOf('/') + 1);
    getGamePage().then(text => this.setState({ gameId: gameId, text: text }));
    return getGame(gameId)
      .then(game => {
        this.refreshGameData(game);
        if (game.archived) {
          getStats(gameId).then(stats => {
            this.setState({ stats: stats });
          });
        }
      })
      .catch(() => {
        this.props.history.push('/');
      });
  }

  refreshGameData = game => {
    this.setState({
      game: game,
      typedGames: game.matchDtos.filter(match => match.typed)
    });
  };

  closeModal = () => {
    this.setState({
      modal: {
        ...this.state.modal,
        isOpen: false
      }
    });
  };

  handleLeaveButtonClick = () => {
    return signOut(this.state.gameId).then(game => this.refreshGameData(game));
  };
  handleTypeButtonClick = () => {
    this.setState({
      modal: {
        ...this.state.modal,
        type: 'new',
        isOpen: true
      }
    });
  };
  handleInviteButtonClick = () => {
    this.props.history.push(`/conversation/new?gameId=${this.state.gameId}`);
  };
  handleEditButtonClick = () => {
    this.setState({
      modal: {
        ...this.state.modal,
        type: 'edit',
        isOpen: true
      }
    });
  };

  handleEditModalClick = event => {
    this.setState({
      modal: {
        ...this.state.modal,
        type: `${event}-modal`,
        isOpen: true
      }
    });
  };

  getStats() {
    if (this.state.stats) {
      const stats = Object.keys(this.state.stats.matchStatusDtos).map(key => ({
        ...this.state.stats.matchStatusDtos[key]
      }));
      return stats.map((stat, index) => <Stats key={index} stats={stat} />);
    }
    return <div />;
  }

  render() {
    const isArchived = this.state.game.archived || moment.unix(this.state.game.endDate).isBefore(moment());
    return (
      <Fragment>
        <div className={style['image-container']}>
          <div className={style.image}>
            <img src={this.state.game.imageUrl} alt="game" />
          </div>
          <div className={style['back-button-container']}>
            <BackButton className={style['back-button']} onClick={this.props.history.goBack} />
          </div>
        </div>
        <div className={style.container}>
          <Modal
            isOpen={this.state.modal.isOpen}
            onRequestClose={this.closeModal}
            className={style.modal}
            contentLabel="Game Modal"
          >
            <ModalSelector
              modalType={this.state.modal.type}
              onClose={this.closeModal}
              game={this.state.game}
              text={this.state.text}
              getUserType={this.state.userType}
              handleEditModalClick={this.handleEditModalClick}
              refreshGameData={this.refreshGameData}
              signIn={this.signIn}
            />
          </Modal>
          <div>
            {isArchived ? (
              <div className={style.archived}>
                <h1>{this.state.text.archived}</h1>
              </div>
            ) : null}
            <div className={style.title}>
              <h1>{this.state.game.title}</h1>
            </div>
            <div className={style.description}>
              <p>{this.state.game.description}</p>
            </div>
            <InfoContainer text={this.state.text} game={this.state.game} />
            {!isArchived ? (
              <div className={style['button-container']}>
                <GameButtons
                  userType={this.state.userType}
                  text={this.state.text}
                  onJoin={this.handleTypeButtonClick}
                  onInvite={this.handleInviteButtonClick}
                  onLeave={this.handleLeaveButtonClick}
                  onType={this.handleTypeButtonClick}
                  onEdit={this.handleEditButtonClick}
                />
              </div>
            ) : null}
            {this.state.typedGames.length !== 0 && !isArchived ? (
              <div className={style['typings-container']}>
                <TypingsShower header={this.state.text.typingsHeader} matches={this.state.typedGames} />
              </div>
            ) : null}
            {isArchived ? <div className={style['stats-container']}>{this.getStats()}</div> : null}
          </div>
        </div>
      </Fragment>
    );
  }
}
