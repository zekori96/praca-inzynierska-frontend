import React, { Component } from 'react';
import style from './Ranking.module.scss';
import { getRankingPage, getRankingRequest } from '../../services/RankingPageService/RankingPageService';
import RankingTile from '../../components/Ranking/RankingTile/RankingTile';
import RankingButtons from '../../components/Ranking/RankingButtons/RankingButtons';
import Input from '../../components/shared/Input/Input';

export class Ranking extends Component {
  state = {
    text: {
      search: {
        button: {}
      },
      user: {}
    },
    ranking: []
  };

  componentDidMount() {
    return Promise.all([getRankingPage(), getRankingRequest()]).then(results =>
      this.setState({ text: results[0], ranking: results[1] })
    );
  }

  handleRankingFilter = (login, typeId = this.state.selectedType) => {
    getRankingRequest(login, typeId).then(ranking =>
      this.setState({ ranking: ranking, searchBox: login, selectedType: typeId })
    );
  };

  render() {
    return (
      <div className={style.container}>
        <div className={style.search}>
          <div className={style['search-box']}>
            <Input
              type="text"
              changed={event => this.handleRankingFilter(event.target.value)}
              placeholder={this.state.text.search.text}
            />
          </div>

          <RankingButtons handleFilter={this.handleRankingFilter} text={this.state.text.search.button} />
        </div>
        <div className={style.ranking}>
          {this.state.ranking.map((user, index) => (
            <RankingTile key={index} user={user} text={this.state.text.user} />
          ))}
        </div>
      </div>
    );
  }
}
