import React from 'react';
import style from './BurgerMenu.module.scss';
import Menu from 'react-burger-menu/lib/menus/slide';
import MenuItem from '../MenuItem/MenuItem';

export default props => {
  const onStateChange = state => {
    if (!state.isOpen) {
      props.onClose();
    }
  };
  return (
    <Menu
      className={style.menu}
      overlayClassName={style.overlay}
      isOpen={props.isOpen}
      onStateChange={onStateChange}
      customBurgerIcon={false}
      customCrossIcon={false}
      left
    >
      <div className={style.content}>
        <div className={style['logo-wrapper']}>
          <img src="/assets/logo-white.svg" alt="logo" />
        </div>
        {props.menuItems.map((menuItem, index) => (
          <MenuItem
            key={index}
            selected={menuItem.link === props.selectedItem}
            link={menuItem.link}
            name={menuItem.name}
            onClick={props.onClose}
            icon={menuItem.icon}
          />
        ))}
      </div>
    </Menu>
  );
};
