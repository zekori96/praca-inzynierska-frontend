export function getOnlySetNumberOfFirstLetters(text = '', numberOfLetters) {
  if (text.length < numberOfLetters) {
    return text;
  }
  let spaceIndex = 0;
  while (spaceIndex < text.length) {
    if (text[spaceIndex] === ' ' && spaceIndex >= numberOfLetters) {
      break;
    }
    spaceIndex++;
  }
  let result = text.substring(0, spaceIndex < numberOfLetters + 10 ? spaceIndex : numberOfLetters);
  result = result + '...';
  return result;
}
