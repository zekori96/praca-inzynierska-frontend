import React from 'react';
import style from './EditGameModal.module.scss';
import Button from '../../shared/Button/Button';
import BackButton from '../../shared/BackButton/BackButton';

export default props => {
  return (
    <div className={style.container}>
      <BackButton className={style['back-button']} onClick={props.onClose} />
      <div className={style.buttons}>
        <Button onClick={() => props.handleEditModalClick('new')} text={props.text.button.new} />
        <Button onClick={() => props.handleEditModalClick('edit')} text={props.text.button.edit} />
        <Button
          onClick={() => props.handleEditModalClick('delete')}
          disabled={props.game.users > 0}
          text={props.text.button.delete}
        />
        <Button
          onClick={() => props.handleEditModalClick('end')}
          disabled={props.game.users < 1}
          text={props.text.button.end}
        />
      </div>
    </div>
  );
};
