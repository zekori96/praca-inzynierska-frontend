import React, { Component, Fragment } from 'react';
import style from './Header.module.scss';
import { withRouter } from 'react-router-dom';
import BurgerMenu from '../../components/Header/BurgerMenu/BurgerMenu';
import { getMenu } from '../../services/BurgerMenuService/BurgerMenuService';
import { connect } from 'react-redux';

export class Header extends Component {
  state = { isMobileMenuOpen: false, hasNotification: false, menu: [] };

  componentDidMount() {
    getMenu(this.props.isLoggedIn).then(menu => this.setState({ menu: menu }));
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.isLoggedIn !== this.props.isLoggedIn) {
      getMenu(this.props.isLoggedIn).then(menu => this.setState({ menu: menu }));
    }
  }

  disableScroll() {
    document.body.style.overflow = 'hidden';
  }

  enableScroll() {
    document.body.style.overflow = 'visible';
  }

  openBurgerMenu = () => {
    this.disableScroll();
    this.setState({
      isMobileMenuOpen: true
    });
  };

  closeBurgerMenu = () => {
    this.enableScroll();
    this.setState({
      isMobileMenuOpen: false
    });
  };

  openNotification() {
    //TODO:Implement
  }

  render() {
    return (
      <Fragment>
        <header>
          <div className={style.space} />
          <BurgerMenu
            isOpen={this.state.isMobileMenuOpen}
            onClose={this.closeBurgerMenu}
            menuItems={this.state.menu}
            selectedItem={this.props.location.pathname}
          />
          <div className={style.header}>
            <div className={style.content}>
              <img
                className={style.burger}
                onClick={this.openBurgerMenu}
                src="/assets/burgerIcon.svg"
                alt="Open menu"
              />
              {this.props.isLoggedIn ? (
                <img
                  className={style.notifications}
                  onClick={this.openNotification}
                  src={this.state.hasNotification ? '/assets/notification.svg' : '/assets/notification-empty.svg'}
                  alt="notification menu"
                />
              ) : (
                <div />
              )}
              <div className={style.logo} onClick={() => this.props.history.push('/')}>
                <img src="/assets/logo.svg" alt="logo" />
              </div>
            </div>
          </div>
        </header>
        {this.props.children}
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  const isLoggedIn = !!state.profile.user;

  return {
    isLoggedIn
  };
}

export default withRouter(connect(mapStateToProps)(Header));
