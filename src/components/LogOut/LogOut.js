import { connect } from 'react-redux';
import { logOut } from '../../store/profile/profile.actions';

export default connect(
  null,
  { logOut }
)(props => {
  props.logOut();
  localStorage.removeItem('auth');
  props.history.push('/');
  return null;
});
