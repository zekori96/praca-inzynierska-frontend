import React from 'react';
import { NewGame } from './NewGame';
import { createGame, getNewGamePage } from '../../services/NewGamePageService/NewGamePageService';
import {
  checkIfDateIsCorrect,
  checkIfNumberIsNotBlankAndPositive,
  checkIfStringIsNotBlank
} from '../../services/ValidationService/ValidationService';

jest.mock('../../services/NewGamePageService/NewGamePageService', () => ({
  createGame: jest.fn(() => Promise.resolve('id')),
  getNewGamePage: jest.fn(() => Promise.resolve({ text: 'text' }))
}));
jest.mock('../../services/ValidationService/ValidationService', () => ({
  checkIfDateIsCorrect: jest.fn(string => string !== ''),
  checkIfNumberIsNotBlankAndPositive: jest.fn(string => string !== ''),
  checkIfStringIsNotBlank: jest.fn(string => string !== '')
}));
describe('NewGame', () => {
  let newGame;
  const props = { history: { push: jest.fn() } };
  beforeEach(() => {
    newGame = new NewGame(props);
    newGame.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should call getNewGamePage then add result to state ', async () => {
      await newGame.componentDidMount();
      expect(getNewGamePage).toHaveBeenCalled();
      expect(newGame.setState).toHaveBeenCalledWith({ text: { text: 'text' } });
    });
  });

  describe('componentDidUpdate', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });
    it('should call getPlayoffCreators if numberOfPlayoffs has changed', async () => {
      newGame.state = { inputs: { numberOfPlayoffs: 1 } };
      newGame.getPlayoffCreators = jest.fn();
      await newGame.componentDidUpdate(null, { inputs: { numberOfPlayoffs: 2 } });
      expect(newGame.getPlayoffCreators).toHaveBeenCalled();
    });
    it('should not call getPlayoffCreators if numberOfPlayoffs has not changed', async () => {
      newGame.state = { inputs: { numberOfPlayoffs: 2 } };
      newGame.getPlayoffCreators = jest.fn();
      await newGame.componentDidUpdate(null, { inputs: { numberOfPlayoffs: 2 } });
      expect(newGame.getPlayoffCreators).not.toHaveBeenCalled();
    });
  });

  describe('getPlayoffCreators', () => {
    it('should call addSetNumberOfPlayoffCreators if there is need to create more creators', async () => {
      newGame.state = { inputs: { numberOfPlayoffs: 2 }, playoffCreators: [{}] };
      newGame.addSetNumberOfPlayoffCreators = jest.fn();
      await newGame.getPlayoffCreators();
      expect(newGame.addSetNumberOfPlayoffCreators).toHaveBeenCalledWith(1);
    });
    it('should call deleteSetNumberOfPlayoffCreators if there is need to delete more creators', async () => {
      newGame.state = { inputs: { numberOfPlayoffs: 1 }, playoffCreators: [{}, {}] };
      newGame.deleteSetNumberOfPlayoffCreators = jest.fn();
      await newGame.getPlayoffCreators();
      expect(newGame.deleteSetNumberOfPlayoffCreators).toHaveBeenCalledWith(1);
    });
  });

  describe('addSetNumberOfPlayoffCreators', () => {
    it('should add set number of creators to state', async () => {
      newGame.state = { text: 'text', inputs: { endOfGameDate: 'date' }, playoffCreators: [{ id: 'id' }] };
      newGame.handleInput = jest.fn();
      await newGame.addSetNumberOfPlayoffCreators(2);
      expect(newGame.setState).toHaveBeenCalled();
    });
  });

  describe('deleteSetNumberOfPlayoffCreators', () => {
    it('should delete set number of creators from state', async () => {
      newGame.state = { inputs: { matches: [{ id: 'id' }] }, playoffCreators: [{ id: 'id' }] };
      await newGame.deleteSetNumberOfPlayoffCreators(2);
      expect(newGame.setState).toHaveBeenCalled();
    });
  });

  describe('handleInput', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });
    it('should update match id identifier starts with match-', async () => {
      newGame.state = { inputs: { matches: [{ id: 'id' }] }, playoffCreators: [{ id: 'id' }] };
      await newGame.handleInput('value', 'match-identifier', 'matchId');
      expect(newGame.setState).toHaveBeenCalledWith({ inputs: { matches: [{ identifier: 'value' }] } });
    });
    it("should update value with identifier as key if identifier doesn't starts with match-", async () => {
      newGame.state = { inputs: { matches: [{ id: 'id' }] }, playoffCreators: [{ id: 'id' }] };
      await newGame.handleInput('value', 'identifier', 'matchId');
      expect(newGame.setState).toHaveBeenCalledWith({ inputs: { identifier: 'value', matches: [{ id: 'id' }] } });
    });
  });

  describe('handleNumberOfMatchesChange', () => {
    it('should check if data is correct then if is smaller than 10 should save number of matches to state', async () => {
      await newGame.handleNumberOfMatchesChange({ target: { value: 5 } });
      expect(checkIfNumberIsNotBlankAndPositive).toHaveBeenCalledWith(5);
      expect(newGame.setState).toHaveBeenCalledWith({
        errorMessage: [],
        inputs: { endOfGameDate: null, matches: [], numberOfPlayoffs: 5 }
      });
    });
    it('should check if data is correct then if is bigger than 10 should save 10 matches to state and call appendTooMuchMatchesErrorMessage', async () => {
      newGame.appendTooMuchMatchesErrorMessage = jest.fn();
      await newGame.handleNumberOfMatchesChange({ target: { value: 11 } });
      expect(checkIfNumberIsNotBlankAndPositive).toHaveBeenCalledWith(11);
      expect(newGame.setState).toHaveBeenCalledWith({
        inputs: { endOfGameDate: null, matches: [], numberOfPlayoffs: 10 }
      });
      expect(newGame.appendTooMuchMatchesErrorMessage).toHaveBeenCalled();
    });
    it('should check if data is correct then if is bigger than 10 and error message exists should save 10 matches to state', async () => {
      newGame.appendTooMuchMatchesErrorMessage = jest.fn();
      newGame.state = { errorMessage: [{ key: 'tooMuchMatchesErrorMessage' }] };
      await newGame.handleNumberOfMatchesChange({ target: { value: 11 } });
      expect(checkIfNumberIsNotBlankAndPositive).toHaveBeenCalledWith(11);
      expect(newGame.setState).toHaveBeenCalledWith({ inputs: { numberOfPlayoffs: 10 } });
      expect(newGame.appendTooMuchMatchesErrorMessage).not.toHaveBeenCalled();
    });
  });

  describe('appendTooMuchMatchesErrorMessage', () => {
    it('should add tooMuchMatches error message to state', async () => {
      newGame.state = { errorMessage: [], text: { tooMuchMatchesErrorMessage: null } };
      await newGame.appendTooMuchMatchesErrorMessage();
      expect(newGame.setState).toHaveBeenCalled();
    });
  });

  describe('handleCreateButton', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });
    it('should call createGame with proper data then redirect to game with gameId', async () => {
      newGame.state = {
        inputs: {
          title: 'title',
          description: 'description',
          gameType: 'gameType',
          endOfGameDate: 'endOfGameDate',
          matches: [{ id: 'id' }]
        }
      };
      await newGame.handleCreateButton();
      expect(createGame).toHaveBeenCalledWith({
        description: 'description',
        endDate: 'endOfGameDate',
        matches: [{ id: 'id' }],
        title: 'title',
        type: 'gameType'
      });
      expect(props.history.push).toHaveBeenCalledWith('/game/id');
    });
    it('should call createGame with proper data then add error message to state if createGame return reject', async () => {
      newGame.state = {
        errorMessage: [],
        text: { serverErrorMessage: '' },
        inputs: {
          title: 'title',
          description: 'description',
          gameType: 'gameType',
          endOfGameDate: 'endOfGameDate',
          matches: [{ id: 'id' }]
        }
      };
      createGame.mockReturnValue(Promise.reject());
      jest.spyOn(global.console, 'error').mockImplementation(() => {});
      await newGame.handleCreateButton();
      expect(createGame).toHaveBeenCalledWith({
        description: 'description',
        endDate: 'endOfGameDate',
        matches: [{ id: 'id' }],
        title: 'title',
        type: 'gameType'
      });
      expect(newGame.setState).toHaveBeenCalled();
    });
  });

  describe('canCreateGame', () => {
    it('should return true if data is correct', async () => {
      newGame.state = {
        inputs: {
          title: 'title',
          description: 'description',
          gameType: 'gameType',
          endOfGameDate: 'endOfGameDate',
          numberOfPlayoffs: 2,
          matches: [{ id: 'id' }]
        }
      };
      newGame.checkIfMatchesAreCorrect = jest.fn(matches => matches.length > 0);
      const result = await newGame.canCreateGame();
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('title');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('description');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('gameType');
      expect(checkIfDateIsCorrect).toHaveBeenCalledWith('endOfGameDate');
      expect(newGame.checkIfMatchesAreCorrect).toHaveBeenCalledWith([{ id: 'id' }]);
      expect(result).toEqual(true);
    });
    it('should return false if title is blank', async () => {
      newGame.state = {
        inputs: {
          title: '',
          description: 'description',
          gameType: 'gameType',
          endOfGameDate: 'endOfGameDate',
          numberOfPlayoffs: 2,
          matches: [{ id: 'id' }]
        }
      };
      newGame.checkIfMatchesAreCorrect = jest.fn(matches => matches.length > 0);
      const result = await newGame.canCreateGame();
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('description');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('gameType');
      expect(checkIfDateIsCorrect).toHaveBeenCalledWith('endOfGameDate');
      expect(newGame.checkIfMatchesAreCorrect).toHaveBeenCalledWith([{ id: 'id' }]);
      expect(result).toEqual(false);
    });
    it('should return false if description is blank', async () => {
      newGame.state = {
        inputs: {
          title: 'title',
          description: '',
          gameType: 'gameType',
          endOfGameDate: 'endOfGameDate',
          numberOfPlayoffs: 2,
          matches: [{ id: 'id' }]
        }
      };
      newGame.checkIfMatchesAreCorrect = jest.fn(matches => matches.length > 0);
      const result = await newGame.canCreateGame();
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('title');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('gameType');
      expect(checkIfDateIsCorrect).toHaveBeenCalledWith('endOfGameDate');
      expect(newGame.checkIfMatchesAreCorrect).toHaveBeenCalledWith([{ id: 'id' }]);
      expect(result).toEqual(false);
    });
    it('should return false if gameType is blank', async () => {
      newGame.state = {
        inputs: {
          title: 'title',
          description: 'description',
          gameType: '',
          endOfGameDate: 'endOfGameDate',
          numberOfPlayoffs: 2,
          matches: [{ id: 'id' }]
        }
      };
      newGame.checkIfMatchesAreCorrect = jest.fn(matches => matches.length > 0);
      const result = await newGame.canCreateGame();
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('title');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('description');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('');
      expect(checkIfDateIsCorrect).toHaveBeenCalledWith('endOfGameDate');
      expect(newGame.checkIfMatchesAreCorrect).toHaveBeenCalledWith([{ id: 'id' }]);
      expect(result).toEqual(false);
    });
    it('should return false if endOfGameDate is blank', async () => {
      newGame.state = {
        inputs: {
          title: 'title',
          description: 'description',
          gameType: 'gameType',
          endOfGameDate: '',
          numberOfPlayoffs: 2,
          matches: [{ id: 'id' }]
        }
      };
      newGame.checkIfMatchesAreCorrect = jest.fn(matches => matches.length > 0);
      const result = await newGame.canCreateGame();
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('title');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('description');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('gameType');
      expect(checkIfDateIsCorrect).toHaveBeenCalledWith('');
      expect(newGame.checkIfMatchesAreCorrect).toHaveBeenCalledWith([{ id: 'id' }]);
      expect(result).toEqual(false);
    });
    it('should return false if numberOfPlayoffs is less or equal 0', async () => {
      newGame.state = {
        inputs: {
          title: 'title',
          description: 'description',
          gameType: 'gameType',
          endOfGameDate: 'endOfGameDate',
          numberOfPlayoffs: 0,
          matches: [{ id: 'id' }]
        }
      };
      newGame.checkIfMatchesAreCorrect = jest.fn(matches => matches.length > 0);
      const result = await newGame.canCreateGame();
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('title');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('description');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('gameType');
      expect(checkIfDateIsCorrect).toHaveBeenCalledWith('endOfGameDate');
      expect(newGame.checkIfMatchesAreCorrect).toHaveBeenCalledWith([{ id: 'id' }]);
      expect(result).toEqual(false);
    });
    it('should return false if matches are not correct', async () => {
      newGame.state = {
        inputs: {
          title: 'title',
          description: 'description',
          gameType: 'gameType',
          endOfGameDate: 'endOfGameDate',
          numberOfPlayoffs: 2,
          matches: []
        }
      };
      newGame.checkIfMatchesAreCorrect = jest.fn(matches => matches.length > 0);
      const result = await newGame.canCreateGame();
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('title');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('description');
      expect(checkIfStringIsNotBlank).toHaveBeenCalledWith('gameType');
      expect(checkIfDateIsCorrect).toHaveBeenCalledWith('endOfGameDate');
      expect(newGame.checkIfMatchesAreCorrect).toHaveBeenCalledWith([]);
      expect(result).toEqual(false);
    });
  });

  describe('checkIfMatchesAreCorrect', () => {
    it('should return true if data is correct', async () => {
      const result = await newGame.checkIfMatchesAreCorrect([
        { date: 'date', team1Name: 'team1Name', team2Name: 'team2Name', title: 'title' }
      ]);
      expect(result).toEqual(true);
    });
    it('should return false if date is not correct', async () => {
      const result = await newGame.checkIfMatchesAreCorrect([
        { date: '', team1Name: 'team1Name', team2Name: 'team2Name', title: 'title' }
      ]);
      expect(result).toEqual(false);
    });
    it('should return false if team1Name is not correct', async () => {
      const result = await newGame.checkIfMatchesAreCorrect([
        { date: 'date', team1Name: '', team2Name: 'team2Name', title: 'title' }
      ]);
      expect(result).toEqual(false);
    });
    it('should return false if team2Name is not correct', async () => {
      const result = await newGame.checkIfMatchesAreCorrect([
        { date: 'date', team1Name: 'team1Name', team2Name: '', title: 'title' }
      ]);
      expect(result).toEqual(false);
    });
    it('should return false if title is not correct', async () => {
      const result = await newGame.checkIfMatchesAreCorrect([
        { date: 'date', team1Name: 'team1Name', team2Name: 'team2Name', title: '' }
      ]);
      expect(result).toEqual(false);
    });
  });
});
