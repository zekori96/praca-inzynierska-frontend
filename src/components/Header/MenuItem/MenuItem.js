import React from 'react';
import style from './MenuItem.module.scss';
import { Link } from 'react-router-dom';

export default props => {
  return props.selected ? (
    <Link className={[style.item, style.selected].join(' ')} to={props.link} onClick={props.onClick}>
      <img src={props.icon.selected} alt="selected menu" />
      <p>{props.name}</p>
    </Link>
  ) : (
    <Link className={style.item} to={props.link} onClick={props.onClick}>
      <img src={props.icon.notSelected} alt="menu" />
      <p>{props.name}</p>
    </Link>
  );
};
