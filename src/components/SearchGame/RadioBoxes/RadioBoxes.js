import React from 'react';
import style from './RadioBoxes.module.scss';

export default props => {
  return (
    <div className={style.sort}>
      <h2>{props.title}</h2>
      <form>
        {props.boxes.map((box, index) => (
          <label className={style.container} key={index}>
            {box.text}
            <input
              type="radio"
              name={props.title}
              onClick={() => props.handle(box.identifier)}
              defaultChecked={props.currentValue === box.identifier}
            />
            <span className={style['check-mark']} />
          </label>
        ))}
      </form>
    </div>
  );
};
