#!/bin/sh

script_name=$0

if [ $# -lt 2 ];
then
    printf '\nThis script remotely creates all folders for deployment environments. You have to configure your (sub-)domains to point to these folders.\n\n'
    printf 'Usage: %s <ssh-login> <project-root-path>\n' "${script_name}"
    printf 'Example: %s www@example.org /www/htdocs/my.domain\n' "${script_name}"
    exit 1
fi

ssh_login="${1}"
project_root_path="${2}"

branches="master release stage develop" 

ssh "${ssh_login}" 'for branch in '"${branches}"'; do mkdir -pv '"${project_root_path}"'/${branch}/dummy; ln -sv '"${project_root_path}"'/${branch}/dummy '"${project_root_path}"'/${branch}/current; done'

