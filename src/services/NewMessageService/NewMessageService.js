import { getContentfulEntry } from '../contentfulClient';
import { instance } from '../AxiosService/AxiosService';

export function getNewMessagePage() {
  return getContentfulEntry('1G9b5lI04o2I6CQKsoQQqS').then(({ fields }) => {
    return {
      search: fields.search
    };
  });
}

export function getUsers(searchValue) {
  return instance.get('/users', { params: { login: searchValue } }).then(response => {
    return response.data;
  });
}

export function getConversation(userId) {
  return instance.get(`/conversation/with/${userId}`).then(response => {
    return response.data;
  });
}

export function sendInvite(userId, gameId) {
  return instance.get(`/conversation/invite/${userId}`, { params: { gameId: gameId } }).then(response => {
    return response.data;
  });
}
