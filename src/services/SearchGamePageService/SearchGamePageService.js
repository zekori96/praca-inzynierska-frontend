import { getContentfulEntry } from '../contentfulClient';
import { instance } from '../AxiosService/AxiosService';

export function getSearchGamePage() {
  return getContentfulEntry('1H6ggaDQ6Ye2co8YQaEAea').then(({ fields }) => ({
    text: {
      created: fields.createdText,
      creator: fields.creatorText,
      points: fields.pointsText,
      endDate: fields.endDateText,
      userCount: fields.userCountText,
      search: {
        placeholder: fields.searchBoxPlaceholderText
      },
      sort: {
        text: fields.sortText
      },
      filter: {
        text: fields.filterText
      },
      order: fields.order
    },
    sortBoxes: [
      { identifier: 'score', text: fields.sortByScore, selected: true },
      { identifier: 'name', text: fields.sortByName, selected: false },
      { identifier: 'type', text: fields.sortByType, selected: false },
      { identifier: 'endDate', text: fields.sortByEndDate, selected: false },
      { identifier: 'creator', text: fields.sortByOwner, selected: false },
      { identifier: 'participantsNumber', text: fields.sortByParticipantsNumber, selected: false }
    ],
    filterBoxes: [
      { identifier: 'title', text: fields.filterByTitle, selected: true },
      { identifier: 'type', text: fields.filterByType, selected: false },
      { identifier: 'creator', text: fields.filterByOwner, selected: false },
      { identifier: 'participant', text: fields.filterByParticipant, selected: false }
    ],
    sortTypeBoxes: [
      { identifier: 'descending', text: fields.descending, selected: true },
      { identifier: 'ascending', text: fields.ascending, selected: false }
    ]
  }));
}

export function getGames(searchValue, sortId, filterId, sortTypeId) {
  return instance
    .get('/games', {
      params: {
        searchValue: searchValue,
        sortId: sortId,
        filterId: filterId,
        sortTypeId: sortTypeId
      }
    })
    .then(response => response.data);
}
