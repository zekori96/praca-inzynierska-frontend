import React from 'react';
import { Header } from './Header';
import { getMenu } from '../../services/BurgerMenuService/BurgerMenuService';

jest.mock('../../services/BurgerMenuService/BurgerMenuService', () => ({
  getMenu: jest.fn(() => Promise.resolve({ text: 'text' }))
}));
describe('Header', () => {
  let header;
  const props = {
    isLoggedIn: true
  };
  beforeEach(() => {
    header = new Header(props);
    header.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should call getMenu then add returned value to state', async () => {
      await header.componentDidMount();
      expect(getMenu).toHaveBeenCalledWith(props.isLoggedIn);
      expect(header.setState).toHaveBeenCalledWith({ menu: { text: 'text' } });
    });
  });

  describe('componentDidUpdate', () => {
    it('should call getMenu then add returned value to state if isLoggedIn have changed', async () => {
      await header.componentDidUpdate({ isLoggedIn: false });
      expect(getMenu).toHaveBeenCalledWith(props.isLoggedIn);
      expect(header.setState).toHaveBeenCalledWith({ menu: { text: 'text' } });
    });
  });

  describe('openBurgerMenu', () => {
    it('should disable scroll then set mobile menu state as open', async () => {
      header.disableScroll = jest.fn();
      await header.openBurgerMenu();
      expect(header.disableScroll).toHaveBeenCalled();
      expect(header.setState).toHaveBeenCalledWith({ isMobileMenuOpen: true });
    });
  });

  describe('closeBurgerMenu', () => {
    it('should enable scroll then set mobile menu state as closed', async () => {
      header.enableScroll = jest.fn();
      await header.closeBurgerMenu();
      expect(header.enableScroll).toHaveBeenCalled();
      expect(header.setState).toHaveBeenCalledWith({ isMobileMenuOpen: false });
    });
  });
  describe('openNotification', () => {
    //TODO: Create tests when functionality is done.
  });
});
