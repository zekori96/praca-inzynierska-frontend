import React from 'react';
import { getConversationPage, getMessages, sendNewMessage } from './ConversationService';
import { instance } from '../AxiosService/AxiosService';
jest.mock('../contentfulClient', () => ({
  getContentfulEntry: jest.fn(() =>
    Promise.resolve({
      fields: {
        createMessagePlaceholder: 'createMessagePlaceholder',
        refreshText: 'refreshText',
        send: { fields: { file: { url: 'url' } } }
      }
    })
  )
}));
jest.mock('../AxiosService/AxiosService', () => ({
  instance: {
    get: jest.fn(() => Promise.resolve({ data: 'data' })),
    post: jest.fn(() => Promise.resolve({ data: 'data' }))
  }
}));
describe('ConversationService', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  describe('getConversationPage', () => {
    it('should call getContentfulEntries and returned mapped version', async () => {
      const result = await getConversationPage();
      expect(result).toEqual({ placeholder: 'createMessagePlaceholder', refresh: 'refreshText', send: 'url' });
    });
  });
  describe('getMessages', () => {
    it('should request data from backend and return received data.', async () => {
      const result = await getMessages('id');
      expect(instance.get).toHaveBeenCalledWith('/conversation/id');
      expect(result).toEqual('data');
    });
  });
  describe('sendNewMessage', () => {
    it('should request data from backend and return received data', async () => {
      const result = await sendNewMessage('id', { newMessage: 'newMessage' });
      expect(instance.post).toHaveBeenCalledWith('/conversation/id', {
        invitation: false,
        message: { newMessage: 'newMessage' }
      });
      expect(result).toEqual({ data: 'data' });
    });
  });
});
