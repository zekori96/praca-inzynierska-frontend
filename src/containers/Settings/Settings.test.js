import React from 'react';
import { Settings } from './Settings';
import {
  deleteProfile,
  getSettingsPage,
  sendEditedProfile
} from '../../services/SettingsPageService/SettingsPageService';

jest.mock('../../services/SettingsPageService/SettingsPageService', () => ({
  getSettingsPage: jest.fn(() => Promise.resolve({ text: 'text' })),
  deleteProfile: jest.fn(() => Promise.resolve()),
  sendEditedProfile: jest.fn(() => Promise.resolve({ id: 'id' }))
}));
describe('Settings', () => {
  let settings;
  const props = { email: 'email', logIn: jest.fn(), logOut: jest.fn(), history: { push: jest.fn() } };
  beforeEach(() => {
    settings = new Settings(props);
    settings.setState = jest.fn();
  });

  describe('componentDidMount', () => {
    it('should call getSettings page then add result to state ', async () => {
      await settings.componentDidMount();
      expect(getSettingsPage).toHaveBeenCalled();
      expect(settings.setState).toHaveBeenCalledWith({
        delete: { button: {} },
        errorMessage: [],
        modal: { isOpen: false, showEdit: false },
        settings: { button: {}, text: { email: {}, login: {}, score: {} } },
        text: 'text'
      });
    });
  });

  describe('updateProfile', () => {
    it('should call sendEditedProfile with email,password and old password then logIn user and close modal', async () => {
      settings.closeModal = jest.fn();
      await settings.updateProfile('email', 'password', 'oldPassword');
      expect(sendEditedProfile).toHaveBeenCalledWith({ newPassword: 'password', oldPassword: 'oldPassword' });
      expect(props.logIn).toHaveBeenCalledWith({ id: 'id' });
      expect(settings.closeModal).toHaveBeenCalled();
    });
  });

  describe('closeModal', () => {
    it('should set modal state as close', async () => {
      await settings.closeModal();
      expect(settings.setState).toHaveBeenCalledWith({ modal: { isOpen: false, showEdit: false } });
    });
  });

  describe('handleDelete', () => {
    it('should call deleteProfile then delete auth token, logOut and redirect to main page', async () => {
      await settings.handleDelete();
      expect(deleteProfile).toHaveBeenCalled();
      expect(props.logOut).toHaveBeenCalled();
      expect(props.history.push).toHaveBeenCalledWith('/');
    });
  });
  describe('handleEditButtonClick', () => {
    it('should set showEdit as true and modal state as open', async () => {
      await settings.handleEditButtonClick();
      expect(settings.setState).toHaveBeenCalledWith({ modal: { showEdit: true, isOpen: true } });
    });
  });
  describe('handleDeleteButtonClick', () => {
    it('should set showEdit as false and modal state as open', async () => {
      await settings.handleDeleteButtonClick();
      expect(settings.setState).toHaveBeenCalledWith({ modal: { showEdit: false, isOpen: true } });
    });
  });
});
