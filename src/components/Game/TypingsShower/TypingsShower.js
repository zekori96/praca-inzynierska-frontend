import React from 'react';
import style from './TypingsShower.module.scss';

export default props => {
  return (
    <div className={style.container}>
      <div className={style.header}>
        <h1>{props.header}</h1>
      </div>
      {props.matches.map((match, index) => (
        <div key={index} className={style.game}>
          <h3 className={style['game-title']}>{match.title}</h3>
          <div className={style['score-holder']}>
            <p>{match.team1Name}</p>
            <div className={style.score}>
              <p>{`${match.team1Score} : ${match.team2Score}`}</p>
            </div>
            <p>{match.team2Name}</p>
          </div>
        </div>
      ))}
    </div>
  );
};
