import React from 'react';
import style from './RankingButtons.module.scss';
import Button from '../../shared/Button/Button';

export default props => {
  return (
    <div className={style['search-buttons']}>
      <Button onClick={() => props.handleFilter('', 0)} text={props.text.today} />
      <Button onClick={() => props.handleFilter('', 1)} text={props.text.thisWeek} />
      <Button onClick={() => props.handleFilter('', 2)} text={props.text.thisMonth} />
      <Button onClick={() => props.handleFilter('', 3)} text={props.text.thisYear} />
      <Button onClick={() => props.handleFilter('', 4)} text={props.text.tenBest} />
      <Button onClick={() => props.handleFilter('', 5)} text={props.text.all} />
    </div>
  );
};
