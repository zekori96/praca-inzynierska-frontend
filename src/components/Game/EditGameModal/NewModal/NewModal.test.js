import React from 'react';
import { addMatchToGame } from '../../../../services/GamePageService/GamePageService';
import NewModal from './NewModal';
import * as moment from 'moment';
import {
  checkIfDateIsCorrect,
  checkIfStringIsNotBlank
} from '../../../../services/ValidationService/ValidationService';
jest.mock('../../../../services/GamePageService/GamePageService', () => ({
  addMatchToGame: jest.fn(() => Promise.resolve({ game: 'game' }))
}));
jest.mock('../../../../services/ValidationService/ValidationService', () => ({
  checkIfStringIsNotBlank: jest.fn(() => true),
  checkIfDateIsCorrect: jest.fn(() => true)
}));
describe('NewModal', () => {
  let newModal;
  const props = {
    game: { id: 'id', matchDtos: [{ matchId: 'id' }] },
    refreshGameData: jest.fn(),
    onClose: jest.fn()
  };
  beforeEach(() => {
    newModal = new NewModal(props);
    newModal.setState = jest.fn();
  });

  describe('handleNewTypingChange', () => {
    it('should add event with identifier as key to state', async () => {
      await newModal.handleNewTypingChange('event', 'identifier');
      expect(newModal.setState).toHaveBeenCalledWith({ newMatch: { identifier: 'event' } });
    });
  });

  describe('handleNewTypingSave', () => {
    it('should call addMatchToGame then refreshGameData with provided game and then close modal', async () => {
      newModal.state = { newMatch: { match: 'match' } };
      await newModal.handleNewTypingSave();
      expect(addMatchToGame).toHaveBeenCalledWith('id', { match: 'match' });
      expect(props.refreshGameData).toHaveBeenCalledWith({ game: 'game' });
      expect(props.onClose).toHaveBeenCalled();
    });
  });

  describe('checkIfMatchIsCorrect', () => {
    it('should return true if all data is correct', async () => {
      const date = moment.now();
      newModal.state = { newMatch: { date: date, team1Name: 'team1Name', team2Name: 'team2Name', name: 'name' } };
      const result = await newModal.checkIfMatchIsCorrect();
      expect(result).toBe(true);
    });
    it('should return false if date is not correct', () => {
      newModal.state = { newMatch: { date: '', team1Name: 'team1Name', team2Name: 'team2Name', name: 'name' } };
      checkIfDateIsCorrect.mockReturnValue(false);
      checkIfStringIsNotBlank.mockReturnValue(true);
      const result = newModal.checkIfMatchIsCorrect();
      expect(result).toBe(false);
    });
    it('should return false if team1Name is not correct', () => {
      const date = moment.now();
      newModal.state = { newMatch: { date: date, team1Name: '', team2Name: 'team2Name', name: 'name' } };
      checkIfDateIsCorrect.mockReturnValue(true);
      checkIfStringIsNotBlank.mockImplementation(string => string !== '');
      const result = newModal.checkIfMatchIsCorrect();
      expect(result).toBe(false);
    });
    it('should return false if team2Name is not correct', () => {
      const date = moment.now();
      newModal.state = { newMatch: { date: date, team1Name: 'team1Name', team2Name: '', name: 'name' } };
      checkIfDateIsCorrect.mockReturnValue(true);
      checkIfStringIsNotBlank.mockImplementation(string => string !== '');
      const result = newModal.checkIfMatchIsCorrect();
      expect(result).toBe(false);
    });
    it('should return false if name is not correct', () => {
      const date = moment.now();
      newModal.state = { newMatch: { date: date, team1Name: 'team1Name', team2Name: 'team2Name', name: '' } };
      checkIfDateIsCorrect.mockReturnValue(true);
      checkIfStringIsNotBlank.mockImplementation(string => string !== '');
      const result = newModal.checkIfMatchIsCorrect();
      expect(result).toBe(false);
    });
  });
});
