import React, { Component } from 'react';
import { getNotFoundPage } from '../../services/NotFoundPageService/NotFoundPageService';
import style from './NotFound.module.scss';
import Button from '../../components/shared/Button/Button';

export class NotFound extends Component {
  state = {
    icon: '',
    text: '',
    button: ''
  };

  componentDidMount() {
    getNotFoundPage().then(state => this.setState(state));
  }

  render() {
    return (
      <div className={style.container}>
        <img className={style.icon} src={this.state.icon} alt="warning" />
        <p className={style.text}>{this.state.text}</p>
        <Button onClick={() => this.props.history.push('/')} text={this.state.button} />
      </div>
    );
  }
}
