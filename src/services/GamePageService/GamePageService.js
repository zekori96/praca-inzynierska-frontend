import { getContentfulEntry } from '../contentfulClient';
import { instance } from '../AxiosService/AxiosService';

export function getGamePage() {
  return getContentfulEntry('GB5Vy3ykAmA4WU6I6qWeg').then(({ fields }) => ({
    created: fields.createdText,
    creator: fields.creatorText,
    archived: fields.archivedText,
    button: {
      join: fields.joinText,
      invite: fields.sendInviteText,
      save: fields.saveButtonText,
      leave: fields.leaveButtonText,
      type: fields.typeButtonText,
      edit: fields.editButtonText,
      acceptChanges: fields.acceptChangesButtonText
    },
    points: fields.pointsText,
    endDate: fields.endDateText,
    users: fields.usersText,
    joinPoints: fields.pointsYouWantToSpendText,
    typingsHeader: fields.typingsHeader,
    editModal: {
      button: {
        new: fields.editModalNewText,
        edit: fields.editModalEditText,
        delete: fields.editModalDeleteText,
        end: fields.editModalEndText
      }
    },
    deleteModal: {
      button: {
        yes: fields.deleteModalYesButtonText,
        no: fields.deleteModalNoButtonText
      },
      warning: fields.deleteModalWarningText,
      icon: fields.warningIcon.fields.file.url
    },
    endModal: {
      button: {
        yes: fields.endModalYesButtonText,
        no: fields.endModalNoButtonText
      },
      warning: fields.endModalWarningText,
      icon: fields.warningIcon.fields.file.url
    },
    placeholder: {
      playoffName: fields.playoffNamePlaceholder,
      team1: fields.team1Placeholder,
      team2: fields.team2Placeholder,
      date: fields.datePlaceholder
    }
  }));
}

export function getGame(gameId) {
  return instance.get(`/game/${gameId}`).then(response => response.data);
}

export function signIn(gameId, data) {
  return instance.put(`/game/${gameId}/join`, data).then(response => response.data);
}
export function getStats(gameId, data) {
  return instance.get(`/game/${gameId}/stats`, data).then(response => response.data);
}

export function signOut(gameId) {
  return instance.put(`/game/${gameId}/leave`).then(response => response.data);
}

export function updateTypings(gameId, data) {
  return instance.put(`/game/${gameId}/typings/update`, data).then(response => response.data);
}

export function addMatchToGame(gameId, data) {
  return instance.post(`/game/${gameId}/match`, data).then(response => response.data);
}

export function updateGameInfo(gameId, data) {
  return instance.put(`/game/${gameId}/info`, data).then(response => response.data);
}

export function deleteGame(gameId) {
  return instance.delete(`/game/${gameId}`);
}

export function endGame(gameId, data) {
  return instance.put(`/game/${gameId}/end`, data).then(response => response.data);
}
