import React from 'react';
import style from './DateTimeSelector.module.scss';
import Datetime from 'react-datetime';

export default props => (
  <Datetime
    dateFormat={'DD.MM.YYYY'}
    timeFormat={'HH:mm'}
    className={style['date-time']}
    onChange={event => props.onChange(event, props.identifier, props.index)}
    inputProps={{ placeholder: props.date }}
    isValidDate={currentDate =>
      (props.enableAfter ? currentDate.isAfter(props.enableAfter) : true) &&
      (props.enableBefore != null ? currentDate.isBefore(props.enableBefore) : true)
    }
    input={true}
  />
);
