import React from 'react';
import style from './PlayoffCreator.module.scss';
import Input from '../../shared/Input/Input';
import DateTimeSelector from '../../shared/DateTimeSelector/DateTimeSelector';
import * as moment from 'moment';

export default props => {
  return (
    <div key={props.index} className={style['playoff-creator']}>
      <div className={style['playoff-title']}>
        <Input
          placeholder={props.text.playoff}
          changed={event => props.onClick(event.target.value, `match-title`, props.index)}
        />
      </div>
      <div className={style['playoff-score']}>
        <Input
          placeholder={props.text.team.team1}
          changed={event => props.onClick(event.target.value, `match-team1Name`, props.index)}
        />
        <div className={style.vs}>
          <span>:</span>
        </div>
        <Input
          placeholder={props.text.team.team2}
          changed={event => props.onClick(event.target.value, `match-team2Name`, props.index)}
        />
      </div>
      <div className={style['playoff-date']}>
        <DateTimeSelector
          onChange={props.onClick}
          date={props.text.playoffDate}
          identifier={'match-date'}
          index={props.index}
          enableAfter={moment()}
          enableBefore={props.endGameDate}
        />
      </div>
    </div>
  );
};
