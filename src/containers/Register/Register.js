import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import style from './Register.module.scss';
import { getRegisterPage, sendRegisterRequest } from '../../services/RegisterPageService/RegisterPageService';
import Input from '../../components/shared/Input/Input';
import { logIn } from '../../store/profile/profile.actions';
import Button from '../../components/shared/Button/Button';
import { checkIfStringIsNotBlank, validateEmail } from '../../services/ValidationService/ValidationService';
import { connect } from 'react-redux';

export class Register extends Component {
  state = {
    header: '',
    description: '',
    errorMessage: [],
    text: {
      input: {
        login: '',
        password: '',
        repeatPassword: '',
        email: ''
      },
      button: { register: '', logIn: '' }
    },
    registerForm: {
      login: '',
      password: '',
      repeatPassword: '',
      email: ''
    }
  };
  form;

  componentDidMount() {
    getRegisterPage().then(state => this.setState(state));
  }

  inputChangedHandler = (event, inputIdentifier) => {
    this.setState({
      registerForm: {
        ...this.state.registerForm,
        [inputIdentifier]: event.target.value
      }
    });
  };
  registerHandler = async event => {
    event.preventDefault();
    await this.clearErrorMessage();
    const isCorrect = this.checkIfFormIsCorrect();
    if (isCorrect.isCorrect) {
      const { login, password, email } = this.state.registerForm;
      await sendRegisterRequest(login, password, email)
        .then(user => {
          this.props.logIn(user);
          this.props.history.push('/');
        })
        .catch(error => {
          this.showServerError();
          console.error(error);
        });
    } else {
      if (isCorrect.type.includes(1)) {
        await this.showPasswordLengthError();
      }
      if (isCorrect.type.includes(2)) {
        await this.showPasswordsNotEqualError();
      }
      if (isCorrect.type.includes(3)) {
        await this.showWrongEmailError();
      }
      this.form.reset();
    }
  };

  checkIfFormIsCorrect() {
    const { login, password, email, repeatPassword } = this.state.registerForm;
    let isCorrect = true;
    const types = [];
    if (
      !(
        checkIfStringIsNotBlank(login) &&
        checkIfStringIsNotBlank(password) &&
        checkIfStringIsNotBlank(email) &&
        checkIfStringIsNotBlank(repeatPassword)
      )
    ) {
      isCorrect = false;
      types.push(4);
    }
    if (password.length < 8) {
      isCorrect = false;
      types.push(1);
    }
    if (password !== repeatPassword) {
      isCorrect = false;
      types.push(2);
    }
    if (!validateEmail(email)) {
      isCorrect = false;
      types.push(3);
    }
    return { isCorrect: isCorrect, type: types };
  }

  clearErrorMessage() {
    this.setState({ errorMessage: [] });
  }

  showServerError() {
    this.setState({ errorMessage: [...this.state.errorMessage, <p>{this.state.serverErrorMessage}</p>] });
  }

  showPasswordLengthError() {
    this.setState({ errorMessage: [...this.state.errorMessage, <p>{this.state.passwordLengthErrorMessage}</p>] });
  }

  showPasswordsNotEqualError() {
    this.setState({
      errorMessage: [...this.state.errorMessage, <p>{this.state.passwordsNotEqualErrorMessage}</p>]
    });
  }

  showWrongEmailError() {
    this.setState({ errorMessage: [...this.state.errorMessage, <p>{this.state.wrongEmailErrorMessage}</p>] });
  }

  render() {
    const { login, password, email, repeatPassword } = this.state.registerForm;
    return (
      <div className={style.container}>
        <h1>{this.state.header}</h1>
        <p>{this.state.description}</p>
        {this.state.errorMessage ? <div className={style.errorMessage}>{this.state.errorMessage}</div> : null}
        <form onSubmit={this.registerHandler} ref={el => (this.form = el)}>
          <Input
            type="text"
            label={this.state.text.input.login}
            changed={event => this.inputChangedHandler(event, 'login')}
          />
          <Input
            type="password"
            label={this.state.text.input.password}
            changed={event => this.inputChangedHandler(event, 'password')}
          />
          <Input
            type="password"
            label={this.state.text.input.repeatPassword}
            changed={event => this.inputChangedHandler(event, 'repeatPassword')}
          />
          <Input
            type="text"
            label={this.state.text.input.email}
            changed={event => this.inputChangedHandler(event, 'email')}
          />
          <Button
            type="submit"
            text={this.state.text.button.register}
            disabled={!login || !password || !email || !repeatPassword}
          />
        </form>
        <Link to={'/logIn'}>{this.state.text.button.logIn}</Link>
      </div>
    );
  }
}

export default withRouter(
  connect(
    null,
    { logIn }
  )(Register)
);
