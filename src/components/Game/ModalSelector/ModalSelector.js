import React, { Fragment } from 'react';
import TypingModal from '../TypingModal/TypingModal';
import EditGameModal from '../EditGameModal/EditGameModal';
import NewModal from '../EditGameModal/NewModal/NewModal';
import EditModal from '../EditGameModal/EditModal/EditModal';
import DeleteModal from '../EditGameModal/DeleteModal/DeleteModal';
import EndModal from '../EditGameModal/EndModal/EndModal';

export default props => {
  switch (props.modalType) {
    case 'new':
      return (
        <TypingModal
          text={props.text}
          game={props.game}
          onClose={props.onClose}
          getUserType={props.getUserType}
          refreshGameData={props.refreshGameData}
          signIn={props.signIn}
        />
      );
    case 'edit':
      return (
        <EditGameModal
          onClose={props.onClose}
          handleEditModalClick={props.handleEditModalClick}
          text={props.text.editModal}
          game={props.game}
        />
      );
    case 'new-modal':
      return (
        <NewModal game={props.game} refreshGameData={props.refreshGameData} onClose={props.onClose} text={props.text} />
      );
    case 'edit-modal':
      return (
        <EditModal
          onClose={props.onClose}
          game={props.game}
          acceptChangesText={props.text.button.acceptChanges}
          refreshGameData={props.refreshGameData}
        />
      );
    case 'delete-modal':
      return <DeleteModal game={props.game} text={props.text.deleteModal} onClose={props.onClose} />;
    case 'end-modal':
      return (
        <EndModal
          game={props.game}
          refreshGameData={props.refreshGameData}
          text={props.text.endModal}
          onClose={props.onClose}
        />
      );
    default:
      return <Fragment />;
  }
};
