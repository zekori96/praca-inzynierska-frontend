import axios from 'axios';

export const instance = axios.create({
  baseURL: process.env.REACT_APP_PROD ? '/api' : 'http://localhost:8080/api',
  timeout: 1000,
  headers: { 'Content-Type': 'application/json' }
});

function errorResponseHandler(error) {
  if (error.response) {
    if (error.response.status === 403) {
      localStorage.removeItem('auth');
    }
  }
  return Promise.reject(error);
}

function requestHandler(config) {
  config.headers.Authorization = localStorage.getItem('auth');
  return config;
}

instance.interceptors.request.use(requestHandler);
instance.interceptors.response.use(response => response, errorResponseHandler);
