import { getContentfulEntry } from '../contentfulClient';

export function getLandingPage() {
  return getContentfulEntry('1biLs5AzREOEAQU0WmmcM0').then(({ fields }) => ({
    content: {
      info: fields.info.map(info => ({
        header: info.fields.header,
        content: info.fields.content
      })),
      bulletPoints: fields.bulletPoints.map(bulletPoint => ({
        header: bulletPoint.fields.header,
        content: bulletPoint.fields.content,
        icon: {
          url: bulletPoint.fields.icon.fields.file.url,
          height: bulletPoint.fields.icon.fields.file.details.image.height,
          width: bulletPoint.fields.icon.fields.file.details.image.width
        }
      })),
      welcomeImages: fields.welcomeImages
        ? fields.welcomeImages.map(image => ({
            url: image.fields.file.url,
            height: image.fields.file.details.image.height,
            width: image.fields.file.details.image.width
          }))
        : null
    }
  }));
}
