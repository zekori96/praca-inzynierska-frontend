import { getContentfulEntry } from '../contentfulClient';
import { instance } from '../AxiosService/AxiosService';

export function getConversationPage() {
  return getContentfulEntry('6tM9qbVIROUgEs2skmgyAU').then(({ fields }) => {
    return {
      placeholder: fields.createMessagePlaceholder,
      refresh: fields.refreshText,
      send: fields.send.fields.file.url
    };
  });
}

export function getMessages(conversationId) {
  return instance.get(`/conversation/${conversationId}`).then(response => {
    return response.data;
  });
}

export function sendNewMessage(conversationId, newMessage) {
  return instance.post(`/conversation/${conversationId}`, { invitation: false, message: newMessage });
}
