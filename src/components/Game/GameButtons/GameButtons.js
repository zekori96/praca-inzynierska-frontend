import React, { Fragment } from 'react';
import Button from '../../shared/Button/Button';

export default props => {
  switch (props.userType()) {
    case 'new':
      return (
        <Fragment>
          <Button onClick={props.onJoin} text={props.text.button.join} />
          <Button onClick={props.onInvite} text={props.text.button.invite} />
        </Fragment>
      );
    case 'participant':
      return (
        <Fragment>
          <Button onClick={props.onLeave} text={props.text.button.leave} />
          <Button onClick={props.onInvite} text={props.text.button.invite} />
          <Button onClick={props.onType} text={props.text.button.type} />
        </Fragment>
      );
    case 'creator':
      return (
        <Fragment>
          <Button onClick={props.onInvite} text={props.text.button.invite} />
          <Button onClick={props.onType} text={props.text.button.type} />
          <Button onClick={props.onEdit} text={props.text.button.edit} />
        </Fragment>
      );
    default:
      return <Fragment />;
  }
};
