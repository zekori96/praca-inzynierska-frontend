import { applyMiddleware, compose, createStore } from 'redux';
import rootReducer from './reducers';
import persistState from 'redux-localstorage';
import { connectRouter, routerMiddleware as createRouterMiddleware } from 'connected-react-router';

export const getStore = history => {
  const routerMiddleware = createRouterMiddleware(history);
  const enhancers = [compose(persistState(['profile']))];

  if (process.env.NODE_ENV === 'development') {
    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;
    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension());
    }
  }

  const composedEnhancers = compose(
    applyMiddleware(routerMiddleware),
    ...enhancers
  );

  const routedReducer = connectRouter(history)(rootReducer);

  return createStore(routedReducer, composedEnhancers);
};
