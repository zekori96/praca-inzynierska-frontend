import React from 'react';
import { LandingPage } from './LandingPage';

describe('LandingPage', () => {
  let landingPage;

  beforeEach(() => {
    landingPage = new LandingPage({});
  });

  describe('getRandomWelcomeImage', () => {
    it('should return empty string if there is no welcome images', () => {
      landingPage.state = { content: { welcomeImages: [] } };
      const result = landingPage.getRandomWelcomeImage();
      expect(result).toBe('');
    });
    it('should return url of one of the welcome images if there are any', () => {
      landingPage.state = { content: { welcomeImages: [{ url: 'url' }] } };
      const result = landingPage.getRandomWelcomeImage();
      expect(result).toBe('url');
    });
  });
});
