import React from 'react';
import style from './ConversationTile.module.scss';
import { getAvatar } from '../../../services/AvatarService/AvatarService';
import * as moment from 'moment';

export default props => {
  return (
    <div className={style.content} onClick={() => props.onClick(props.id)}>
      {props.isNew ? <span className={style['new-message']} /> : null}
      <img className={style.avatar} src={getAvatar(props.userId)} alt="avatar" />
      <div className={style['info-container']}>
        <h2 className={style.login}>{props.login}</h2>
        <p className={style['last-message']}>{props.message}</p>
      </div>
      <div className={style['date-container']}>
        <p className={style.date}>{moment.unix(props.date).format('DD.MM.YYYY HH:mm')}</p>
      </div>
    </div>
  );
};
