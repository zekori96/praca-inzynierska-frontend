import React from 'react';
import { getAvatar } from '../../../services/AvatarService/AvatarService';
import style from './Avatar.module.scss';

export default props => {
  return <img className={style.avatar} src={getAvatar(props.id)} alt="avatar" />;
};
